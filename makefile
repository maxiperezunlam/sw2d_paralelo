example:
	gfortran -ffree-form -fopenmp example.for -o example.exe
example-prof:
	gfortran -ffree-form -fopenmp example.for -pg -O2 -o example.exe
install:
	sudo apt-get install -y libgeos-3.5.0 libgeos-dev libproj-dev mongodb
        gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B11796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
#	curl -sSL https://get.rvm.io | bash
	sudo apt-get install -y software-properties-common
	sudo apt-add-repository -y ppa:rael-gc/rvm
	sudo apt-get update
	sudo apt-get install rvm
	rvm install ruby2.5 && rvm use 2.5
	gem install rgeo
	gem install mongo
	gem install awesome
stackomp:
	ulimit -s unlimited
	OMP_STACKSIZE=4G
sw2d:
	gfortran -fopenmp -ffixed-line-length-none -O2 SW2D.for -o sw2d.exe
sw2d-O3:
	gfortran -fopenmp -ffixed-line-length-none -O2 SW2D.for -o sw2d.exe
sw2d-prof:
	gfortran -pg -fopenmp -ffixed-line-length-none -O2 SW2D.for -o sw2d.exe
	./sw2d.exe
	gprof sw2d.exe > profile.gprof.txt
sw2d-bc:
	gfortran -fopenmp -ffixed-line-length-none -fbounds-check -O2 SW2D.for -o sw2dbc.exe
sw2d-dbg:
	gfortran -fopenmp -ffixed-line-length-none -g -O0 SW2D.for -o sw2ddbg.exe	
dbg:
	./dbg.sh
output:
	./output.sh
save-profiling:
	./profilings.sh
clear-dbg:
	rm -f ASMDBG*
	rm -f PARDBG*
	rm -f DEBUG*
	rm -f ASSEMDBG*
clear-output:
	rm -f chuS*
	rm -f chu*.plt
view-profilings:
	tail profile.txt
	tail ../v09-backup/profile.txt
	
