#!/bin/bash
mkdir -p profilings
num1=$(ls profilings/ | sort -n | tail -n 1 | tr --delete [:alpha:])
num1=$(expr $num1 + 1)
mkdir -p "profilings/profile$num1"
mv --force profile.txt profilings/profile$num1/ 2> /dev/null
mv --force profile.gprof.txt profilings/profile$num1/ 2> /dev/null
