FROM ubuntu:20.04

COPY ./* /app

RUN apt update

RUN apt-get install -y software-properties-common

RUN apt-add-repository -y ppa:rael-gc/rvm

RUN apt update

RUN apt install build-essential -y --no-install-recommends

RUN apt-get install -y make

RUN apt-get install -y libgeos-dev libproj-dev mongodb

RUN apt-get install gpg

#Install RVM
RUN apt-get install -y rvm

#Install ruby dependencies
RUN apt-get install -y ruby ruby-dev
RUN gem install rgeo
RUN gem install mongo
RUN gem install awesome


