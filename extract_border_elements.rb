require "awesome_print"
require "mongo"

debug = false

nodes = {}
nidx = 1
elements = {}
client = Mongo::Client.new([ '127.0.0.1:27017'  ], :database => 'c181_islaDensificada')
db = client.database
collection = client[:elements]

File.open("chucor","r").each_line do |l|
  row = l.split(" ")
  if row != nil && row != "" && row != []
    nodes[nidx] = {x: row.first, y: row.last}
    nidx+=1
  end
end

ap "Cantidad de nodos leidos #{nidx-1}"

eidx = 1
documents = []

File.open("chucon", "r").each_line do |l|
  row = l.split(" ")
  if row != nil && row != "" && row != []
    elements[eidx] = {nodo1: row[0], nodo2: row[1], nodo3: row[2]}
    document = {
      element_number: eidx,
      nodes: {
        node1: {
          number: row[0].to_i,
          x: nodes[row[0].to_i][:x],
          y: nodes[row[0].to_i][:y]
        },
        node2: {
          number: row[1].to_i,
          x: nodes[row[1].to_i][:x],
          y: nodes[row[1].to_i][:y]
        },
        node3: {
          number: row[2],
          x: nodes[row[2].to_i][:x],
          y: nodes[row[2].to_i][:y]
        }
      }
    }
    documents << document
    r = collection.insert_one(document)
    ap "No fue insertado en mongo: #{document}" if r.n < 1
    eidx+=1
  end
end

ap "Cantidad de elementos leidos #{eidx-1}"
ap documents if debug
