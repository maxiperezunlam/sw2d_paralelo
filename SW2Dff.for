      MODULE arrayMod
!     DECLARE DYNAMIC ARRAY WITH PARTITION SIZES
      INTEGER, dimension(:), allocatable :: SPART

!     DECLARE DYNAMIC ARRAY OF ARRAY DATA TYPE
      type darray
            integer, dimension(:), allocatable :: VALUES
      end type darray
!     DECLARE A VARIABLE TO STORE PARTITION INFORMATION 
      type(darray), dimension(:), allocatable :: PART

      END MODULE arrayMod

      PROGRAM SHALLOW
!***********************************************************************
!*                                                                     *
!*                                                                     *
!*                                                                     *
!*                                                                     *
!*     P R O G R A M A   D E   E L E M E N T O S   F I N I T O S       *
!*                                                                     *
!*                                                                     *
!*          E Q U A C O E S   D E   A G U A S  R A S A S               *
!*                                                                     *
!*                                                                     *
!*          M E T O D O   D E   T A Y L O R - G A L E R K I N          *
!*                                                                     *
!*                                                                     *
!*    C O N T R O L E   O T I M O  D O  E S C O A M E N T O  E M       *
!*                                                                     *
!*                                                                     *
!*                   A G U A S   R A S A S                             *
!*                                                                     *
!***********************************************************************
! * RESUMEN DE VARIABLES UTILIZADAS EN EL PROGRAMA ********************************
! EXEMPLO: Este archivo es leido desde la rutina principal (Codigo 4)
!   PNAME Esta variable tiene el prefijo/extension para la lectura de los archivos.
! ENTRADA  Este archivo es leido desde la rutina principal (Codigo 5)
!   NIR Numero de intervalos para una impresion
!   NTR Numero total de impresiones
!   NNM Numero de nodos de la malla
!   NEM Numero de elementos de la malla
!   NTBN Numero de nodos de contorno
!   NSIDC Numero de lados de contorno
!   HMIN Valor de altura topografica minimo
!   HMAX Valor de altura topografica maximo
!   NODO Nodo donde esta localizado un vertedero
!   SIGNIFICADO DOS PARAMETROS
!     NNOS : NUMERO DE NS
!   NELEM : NMERO DE ELEMENTOS
!   NTCC : NUMERO TOTAL DE CONDIES DE CONTORNO DE DIRICHILET
!   NTLC : NUMERO TOTAL DE LADOS DE CONTORNO
!   NELEM3 : 3 X NUMERO DE ELEMENTOS
!     NTEMP: NUMERO DE INTERVALOS DE TEMPO + 1
!     COMIENZO PROGRAMA PRINCIPAL
!     DESCRIPCION DE PARAMETROS
!
!   NNOS NUMERO TOTAL DE NODOS DE LA MALLA
!     NELEM NUMERO TOTAL DE ELEMENTOS DE LA MALLA
!     NTCC NUMERO
!     NTLC NUMERO
!     NELEM3 EL TRIPLE DEL NUMERO TOTAL DE NODOS

      PARAMETER(NNOS=2381,NELEM=4400,NTCC=362,NTLC=362,NELEM3=3*NELEM)
      PARAMETER (NTEMP= 1500)
      USE arrayMod
      
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/TITULO/PNAME,PTNAME
      COMMON/IMP/NIR,NTR,IMAX
      COMMON/NELPA/NNM,NEM,NBC,NTBN,NSIDC,NEMNPE
      COMMON/TEMP/CTIME,DT,DT1
      COMMON/ESCFLU/TOL,VMU,RHOIN,F,CH,HTAU1,HTAU2,AMPL,PERI,GRAV
      COMMON/PARVAR/NPROC,NPART
      
      CHARACTER PNAME*3,PTNAME*6
      DIMENSION CORDX(NNOS),CORDY(NNOS),V1(NNOS),V2(NNOS),H(NNOS),
     .          HTOT(NNOS),CHE(NNOS),SNORMP(NTLC),ITYPBC(NNOS),
     .          ITYPB0(NNOS),PAT(NNOS),V2H(NNOS),V1H(NNOS),
     .          FFI(NNOS),PSIX(NNOS),PSIY(NNOS),GGDH1(NNOS),
     .          GGDH2(NNOS),GDHT1(NNOS),GDHT2(NNOS),GPDR(NNOS),
     .          GDF1(NNOS),GDF2(NNOS),GDPA1(NNOS),GDPA2(NNOS),
     .          GD12(NNOS),GD21(NNOS),GDH1(NNOS),GDH2(NNOS),
     .          GPR(NNOS),GPV1(NNOS),GPV2(NNOS),GDM(NNOS),
     .          GCR(NNOS),GCV1(NNOS),GCV2(NNOS),VX(NNOS),VY(NNOS),
     .          GPRK(NNOS),GPV1K(NNOS),GPV2K(NNOS),HHOT(NNOS),
     .        VXA(NNOS),VYA(NNOS),HHTA(NNOS),GSKEN(NNOS),GSKED(NNOS),
     .          FFI00(NNOS),PSIX00(NNOS),PSIY00(NNOS),FFIA(NNOS),
     .        PSIXA(NNOS),PSIYA(NNOS),GPDV1(NNOS),GPDV2(NNOS),
     .          SENS(NNOS),GUARV1(NNOS,NTEMP),GUARV2(NNOS,NTEMP),
     .        GUARHT(NNOS,NTEMP),KONE(NELEM3),GSDM(NELEM3,3),
     .          GUARPP(NNOS, NTEMP),
     .        ISIDEC(2,NTLC),NORMP(NTLC),SNORMC(NTLC),
     .          V1CO(NTCC),V2CO(NTCC), HC0(NTCC),ICC(NTCC),ICC0(NTCC),
     .          PSIX0(NTCC),PSIY0(NTCC),FFI0(NTCC),DTE(NELEM),
     .        FFIEL(NELEM),PSIXEL(NELEM),PSIYEL(NELEM),ELV1(3),
     .          ELV2(3),GDSF1(3),GDSF2(3),CM(3,3),DM(3),EDF1(3),EDF2(3),
     .          EDPA1(3),EDPA2(3),ED12(3),ED21(3),EDH1(3),EDH2(3),
     .          ELV1H(3),ELV2H(3),EHTOT(3),ELP(3),ELCHE(3),EDHT1(3),
     .          EDHT2(3),PCL1(3),PCL2(3),PCL3(3),F11N(3),F12N(3),
     .          F13N(3),F21N(3),F22N(3),F23N(3),PPL1(3),PPL2(3),PPL3(3),
     .          SDM(3,3),EHTK(3),ELV1K(3),ELV2K(3),PLLL1(3),PLLL2(3),
     .        PLLL3(3),PP(NNOS),SKEN(3),SKED(3),PLLS1(3),PLLS2(3),
     .          ELPSIX(3),ELPSIY(3),ELFFI(3),ELPAT(3),ELH(3),B(3),D(3),
     .        PLLS3(3),FONT1(3),FONT2(3),FONT3(3),ELXY(3,2),ELF1(3,3),
     .          ELF2(3,3)
      real*8 start_time, finish_time
      INTEGER TID

!     For time profiling...
      DOUBLE PRECISION :: ttot
      DOUBLE PRECISION :: tRODA
      DOUBLE PRECISION :: tnoRODA
      DOUBLE PRECISION :: tstart
      DOUBLE PRECISION :: ttick
      DOUBLE PRECISION :: tThreadTick
      DOUBLE PRECISION :: tTotalMainTick
      DOUBLE PRECISION :: tTotalMainLoop
      DOUBLE PRECISION :: tThreadMainLoop
      INTEGER          :: cont

!     Initialize timers & start accounting
      cont = 0
      ttot    = 0.0
      tRODA   = 0.0
      tnoRODA = 0.0
      tstart  = omp_get_wtime()
      ttick   = tstart
                 
      m1 = 1


      start_time = omp_get_wtime()

!     File for writing everything related to timing and profile
      OPEN(1000, FILE="profile.txt")

!   LEITURA DE DADOS
!   LEEMOS EL PREFIJO/EXTENSION A UTILIZAR PARA LA LECTURA LOS ARCHIVOS DE DATOS

      OPEN(4, FILE = 'exemplo',status ='old',action='read')
      READ(4,*)PNAME
      CLOSE(4)
      OPEN(5, FILE = 'entrada.'//PNAME)
      OPEN(6, FILE = 'saida.'//PNAME)
      OPEN(6666, FILE = 'ERROR.LOG')

!     LEEMOS LOS DATOS DESDE EL ARCHIVO entrada.chu (o lo configurado en EXEMPLO)
!     PRIMER LINEA DEL ARCHIVO ENTRADA.CHU
      READ(5,*) PNAME
!     SEGUNDA LINEA DEL ARCHIVO ENTRADA.CHU
      READ(5,*) NIR,NTR,NNM,NEM,NTBN,NSIDC,HMIN
!     TERCER LINEA DEL ARCHIVO ENTRADA.CHI
      READ(5,*) HMAX, NODO
!   ASIGNAMOS LOS VALORES DE ...
      NFILE=1
      ITERM = 0
      NNR = 1
      NNK = 0

      WRITE(6,*) 'C O N T R O L E   D E  D A D O S'
      WRITE(6,*)
      WRITE(6,1000) PNAME
 1000 FORMAT(///1X,'PROBLEMA ',A3//)
      WRITE(6,2000) NIR,NTR,NNM,NEM,NTBN,NSIDC,HMIN,HMAX,NODO
 2000 FORMAT(/10X,' NUMERO DE INTERVALOS PARA UMA IMPRESSO  =',I10,
     .       /10X,' NUMERO TOTAL DE IMPRESSES       =',I10,
     .       /10X,' NUMERO DE NS DA MALHA                  =',I10,
     .       /10X,' NUMERO DE ELEMENTOS DA MALHA             =',I10,
     .       /10X,' NUMERO DE NS DE CONTORNO                   =',I10,
     .       /10X,' NUMERO DE LADOS DE CONTORNO               =',I10,
     .       /10X,'VALOR MNIMO ADMISSVEL DA PROFUNDIDADE =',F14.6,
     .       /10X,'VALOR MXIMO ADMISSVEL DA PROFUNDIDADE =',F14.6,
     .       /10X,'NODO ONDE EST LOCALIZADO UM VERTEDOURO', I6)

!    LEEMOS TODOS LOS DATOS DESDE EL RESTO DE LOS ARCHIVOS DE ENTRADA

      CALL RDATA(KONE,CORDX,CORDY,V1,V2,H,HTOT,PP,FFI,PSIX,PSIY,ICC,
     .  ICC0,ISIDEC,NORMP,ITYPBC,ITYPB0,SNORMP,SNORMC,CC,CC1,PAT,V1CO,
     .   V2CO,HC0,FFI0,PSIX0,PSIY0,NNOS,NTCC,NTLC,NELEM3,
     .  SPART, PART)

!  FIN DE LA LECTURA DE DATOS

!    CALCULO DEL PASO DE TIEMPO PARA QUE NO OCURRA INESTABILIDAD
!    NUMERICA, SE CALCULA EL SEMI PASO DE TIEMPO Y EL NUMERO DE INTERVALOS
!    DE TIEMPO
!    DT PASO DE TIEMPO
!    DT1 SEMIPASO DE TIEMPO
!    NUMERO DE INTERVALOS DE TIEMPO NIRNTR
      CALL CDT(CORDX,CORDY,KONE,V1,V2,HTOT,NNOS,NELEM3)
      DT1=0.5D0*DT
      NIRNTR=NIR*NTR
      NTEMPO=NIRNTR+1

!    ALMACENAMIENTO DE LAS COMPONENTES DE LAS VELOCIDADES Y PROFUNDIDADES TOTALES
!    EN EL TIEMPO INICIAL
!    SI m1 es 2 Calcula la solucion adjunta y la directa, sino solo la directa
      if (m1 .EQ. 2)        THEN
!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
        DO I=1,NNM
          GUARV1(I,NTEMPO)=V1(I)
          GUARV2(I,NTEMPO)=V2(I)
          GUARHT(I,NTEMPO)=HTOT(I)
          GDM(I)= 0.D0
          GDF1(I) = 0.D0
          GDF2(I)= 0.D0
          GD12(I) = 0.D0
          GD21(I) = 0.D0
          GDPA1(I) = 0.D0
          GDPA2(I)= 0.D0
          GDH1(I) = 0.D0
          GDH2(I) = 0.D0
        END DO
!!$OMP END DO
!!$OMP END PARALLEL
      ELSE
!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
        DO I = 1, NNM
          GDM(I)= 0.D0
          GDF1(I) = 0.D0
          GDF2(I)= 0.D0
          GD12(I) = 0.D0
          GD21(I) = 0.D0
          GDPA1(I) = 0.D0
          GDPA2(I)= 0.D0
          GDH1(I) = 0.D0
          GDH2(I) = 0.D0
        END DO
!!$OMP END DO
!!$OMP END PARALLEL
      ENDIF

!    CALCULO DA FUN??O DE FORMA E DE SUAS DERIVADAS.
!    SUAVIZA??O DAS DERIVADAS DE ALGUMAS VARIAVEIS.

      DO NE=1,NEM
        CALL SELXY(NE,KONE,CORDX,CORDY,ELXY,NNOS,NELEM3)
        CALL ELEM(NE,KONE,V1,V2,PAT,H,ELV1,ELV2,ELPAT,ELH,0,NNOS,
     .            NELEM3)
        CALL SHAPET(ELXY,GDSF1,GDSF2,DET,B,D)
        CALL MASSTD(DET,CM,DM)
        CALL VETORT(GDSF1,GDSF2,DET,ELV1,ELV2,EDF1,EDF2,
     .              EDPA1,EDPA2,ELH,ED12,ED21,EDH1,EDH2,ELPAT)
        CALL ASSEM(NE,GDM,DM,KONE,NNOS,NELEM3)
        CALL ASSEM(NE,GDF1,EDF1,KONE,NNOS,NELEM3)
        CALL ASSEM(NE,GDF2,EDF2,KONE,NNOS,NELEM3)
        CALL ASSEM(NE,GD12,ED12,KONE,NNOS,NELEM3)
        CALL ASSEM(NE,GD21,ED21,KONE,NNOS,NELEM3)
        CALL ASSEM(NE,GDPA1,EDPA1,KONE,NNOS,NELEM3)
        CALL ASSEM(NE,GDPA2,EDPA2,KONE,NNOS,NELEM3)
        CALL ASSEM(NE,GDH1,EDH1,KONE,NNOS,NELEM3)
        CALL ASSEM(NE,GDH2,EDH2,KONE,NNOS,NELEM3)
      END DO

!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
      DO I=1,NNM
        GDF1(I) = GDF1(I)/GDM(I)
        GDF2(I) = GDF2(I)/GDM(I)
        GD12(I) = GD12(I)/GDM(I)
        GD21(I) = GD21(I)/GDM(I)
        GDPA1(I) = GDPA1(I)/GDM(I)
        GDPA2(I) = GDPA2(I)/GDM(I)
        GDH1(I) = GDH1(I)/GDM(I)
        GDH2(I) = GDH2(I)/GDM(I)
        GGDH1(I) = GDH1(I)
        GGDH2(I) = GDH2(I)
      ENDDO
!!$OMP END DO
!!$OMP END PARALLEL


!    INICIO DO PASSO DO TEMPO

      DO ITEMPO=1,NIRNTR
        CTIME=CTIME+DT
!no se puede unificar con el ciclo de arriba. Al probarlo no ejecutaba
        
        DO I=1,NNM
          GPR(I)=0.0D0
          GPV1(I)=0.0D0
          GPV2(I)=0.0D0
          GDM(I)=0.0D0
          GCR(I)=0.0D0
          GCV1(I)=0.0D0
          GCV2(I)=0.0D0
!La friccion en el cause se mantiene constante segun nuestras suposiciones
          CHE(I)=CH
        ENDDO

        CALL MOVIM(V1,V2,HTOT,V1H,V2H,NNOS)

!  ATRIBUI??O DO COEFICIENTE DE CHEZY A CADA NODO.
!  O COEFICIENTE "CH" PODE SER UMA FUN??O DO COEFICIENTE
!  DE MANNING E DA PROFUNDIDADE. POR ESTA RAZO FOI COLOCADO NO
!    "LOOP" DOS PASSOS DE TEMPO. NESTE CASO CH=CH(COEF DE MANNING)
!    E DEVE SER MULTIPLICADO POR HTOT(I)**1/6.


!           CALCULO DAS VARIVEIS NO TEMPO N+1/2
!Medimos el tiempo total de cpu en ejecutar este loop para cada ciclo temporal
        tTotalMainTick = omp_get_wtime()
        DO NE=1,NEM
          CALL SELXY(NE,KONE,CORDX,CORDY,ELXY,NNOS,NELEM3)
          CALL ELEM(NE,KONE,V1H,V2H,PP,HTOT,ELV1H,ELV2H,ELP,
     .              EHTOT,0,NNOS,NELEM3)
          CALL ELEM(NE,KONE,V1,V2,PP,HTOT,ELV1,ELV2,ELP,EHTOT,1,
     .              NNOS,NELEM3)
          CALL ELEMH(NE,KONE,H,ELH,NNOS,NELEM3)
          CALL ELEMH(NE,KONE,GDPA1,EDPA1,NNOS,NELEM3)
          CALL ELEMH(NE,KONE,GDPA2,EDPA2,NNOS,NELEM3)
          CALL ELEMH(NE,KONE,GDH1,EDH1,NNOS,NELEM3)
          CALL ELEMH(NE,KONE,GDH2,EDH2,NNOS,NELEM3)
          CALL ELEMH(NE,KONE,CHE,ELCHE,NNOS,NELEM3)
          CALL ELEMD(NE,KONE,GDF1,GDF2,GD12,GD21,EDF1,EDF2,ED12,ED21,
     .               NNOS,NELEM3)
          CALL SHAPET(ELXY,GDSF1,GDSF2,DET,B,D)
          CALL MASSTC(DET,CM,EHTOT,ELV1H,ELV2H,PCL1,PCL2,PCL3)
          CALL CALFT(GDSF1,GDSF2,DET,ELV1,ELV2,ELP,EHTOT,ELV1H,
     .               ELV2H,F11,F12,F13,F21,F22,F23,F12N,F23N,
     .               F13N,F22N,EDF1,EDF2,ED12,ED21,U1,U2,U3,
     .               U5,U6,U7,FONT1,FONT2,FONT3,EDPA1,EDPA2,EDH1,
     .               EDH2,FNTM1,FNTM2,FNTM3,ELH,ELCHE)


!      CALCULOS DAS VARIAVEIS NO TEMPO N+1
          CALL FLVT(F11,F12,F13,F21,F22,F23,GDSF1,GDSF2,FNTM1,
     .              FNTM2,FNTM3,PPL1,PPL2,PPL3,DET)
          CALL ASSEM(NE,GPR,PPL1,KONE,NNOS,NELEM3)
          CALL ASSEM(NE,GPV1,PPL2,KONE,NNOS,NELEM3)
          CALL ASSEM(NE,GPV2,PPL3,KONE,NNOS,NELEM3)
          CALL ASSEM(NE,GCR,PCL1,KONE,NNOS,NELEM3)
          CALL ASSEM(NE,GCV1,PCL2,KONE,NNOS,NELEM3)
          CALL ASSEM(NE,GCV2,PCL3,KONE,NNOS,NELEM3)
          CALL BOUFOR(NE,CORDX,CORDY,KONE,ISIDEC,GPR,GPV1,GPV2,
     .                F11,F12,F13,F21,F22,F23,ELV1H,F12N,F13N,
     .                ELV2H,F22N,F23N,SNORMC,NNOS,NELEM3,NTLC)
          CALL MASST(DET,CM,DM,SDM)
          CALL ASSEM(NE,GDM,DM,KONE,NNOS,NELEM3)
          NE1=(NE-1)*3
          CALL MONTA1(GSDM,SDM,NE1,NELEM3)
        END DO
        tTotalMainLoop = tTotalMainLoop + 
     .                   (omp_get_wtime() - tTotalMainTick)
         
        ITER=0

        CALL INICIOLO(HTOT,V1H,V2H,VX,VY,HHOT,NNM,NNOS)

!    INICIO DO LA?O ITERATIVO

        DO WHILE (ITER.EQ.0.OR.(TOL1.GT.TOL.OR.TOL2.GT.TOL))
          SUM1=0.0D0
          SUM2=0.0D0
          SUM3=0.0D0
          SUM5=0.0D0
          SUM6=0.0D0
          SUM7=0.0D0

!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
          DO I=1,NNM
            GPRK(I)=0.0D0
            GPV1K(I)=0.0D0
            GPV2K(I)=0.0D0
          ENDDO
!!$OMP END DO
!!$OMP END PARALLEL

          ITER=ITER+1

          DO NE=1,NEM
            NE1=(NE-1)*3
            CALL MONTA2(GSDM,SDM,NE1,NELEM3)

            CALL ELEMK(NE,KONE,HHOT,VX,VY,EHTK,ELV1K,ELV2K,
     .                 NNOS,NELEM3)
            CALL DMLVTQ(EHTK,ELV1K,ELV2K,SDM,PLLL1,PLLL2,PLLL3)
            CALL ASSEM(NE,GPRK,PLLL1,KONE,NNOS,NELEM3)
            CALL ASSEM(NE,GPV1K,PLLL2,KONE,NNOS,NELEM3)
            CALL ASSEM(NE,GPV2K,PLLL3,KONE,NNOS,NELEM3)
          END DO

          CALL CONDICA(V1H,V2H,HTOT,GPR,GPV1,GPV2,GDM,GPRK,
     .                 GPV1K,GPV2K,GCR,GCV1,GCV2,VX,VY,PP,
     .                 HHOT,VXA,VYA,HHTA, ITYPBC,H, ITER,HMIN,NNOS)


!  AS VARIVEIS FORAM CALCULADAS NO TEMPO N+1, NA ITERA??O K+1.
!  INICIA-SE O PROCESSO DE APLICAR UM MECANISMO PARA ELIMINAR
!  OSCILA??ES EM ZONAS ONDE HA ONDAS DE CHOQUE, SUAVIZANDO AS
!    VARIVEIS.

          IF (CC.GT.1.E-06)THEN
            CALL CDT1(CORDX,CORDY,KONE,V1,V2,HTOT,DTE,
     .                NNOS,NELEM3,NELEM)

!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
            DO I=1,NNM
              GPDR(I)=0.0D0
              GPDV1(I)=0.0D0
              GPDV2(I)=0.0D0
              GSKEN(I)=0.0D0
              GSKED(I)=0.0D0
            ENDDO
!!$OMP END DO
!!$OMP END PARALLEL

            DO NE=1,NEM
              NE1=(NE-1)*3
              CALL MONTA2(GSDM,SDM,NE1,NELEM3)
              CALL ELEMP(NE,KONE,PP,ELP,PAT,NNOS,NELEM3)
              CALL SKELEM(SDM,ELP,SKEN,SKED)
              CALL ASSEM(NE,GSKEN,SKEN,KONE,NNOS,NELEM3)
              CALL ASSEM(NE,GSKED,SKED,KONE,NNOS,NELEM3)
            END DO

            CALL DIVI(GSKEN,GSKED,NNOS)

            DO NE=1,NEM
              CALL ELEMS(NE,KONE,GSKEN,SM,NNOS,NELEM3)
              NE1=(NE-1)*3
              CALL MONTA2(GSDM,SDM,NE1,NELEM3)
              CALL ELEM(NE,KONE,VX,VY,PP,HHOT,ELV1K,ELV2K,
     .                  ELP,EHTK,0,NNOS,NELEM3)
              CALL DMLCFL(ELV1K,ELV2K,EHTK,SDM,PLLS1,PLLS2,
     .                    PLLS3,DTE,SM,NE,CC,NELEM)
              CALL ASSEM(NE,GPDR,PLLS1,KONE,NNOS,NELEM3)
              CALL ASSEM(NE,GPDV1,PLLS2,KONE,NNOS,NELEM3)
              CALL ASSEM(NE,GPDV2,PLLS3,KONE,NNOS,NELEM3)
            END DO

            CALL SMOOTH(VX,VY,HHOT,GPDR,GPDV1,GPDV2,GDM,
     .                  ITYPBC,HMIN,NNOS)
          END IF


!  FIM DO PROCESSO DE SUAVIZA?AO
!  ANALISE DA CONVERG?NCIA DO PROCESSO ITERATIVO

          CALL CONVERG(VX,VY,HHOT,VXA,VYA,HHTA,SUM1,SUM2,SUM3,
     .        SUM5,SUM6,SUM7,NNOS)
          DEUCL=DSQRT(SUM1+SUM2)
          EUCL=DSQRT(SUM5+SUM6)

          IF (DABS(EUCL).LT.0.1E-06) THEN
            TOL1=DEUCL
          ELSE
            TOL1 = DEUCL / EUCL
          ENDIF

          DEUCL=DSQRT(SUM3)
          EUCL = DSQRT(SUM7)

          IF (DABS(EUCL).LT.0.1E-06) THEN
            TOL2=DEUCL
          ELSE
            TOL2 = DEUCL / EUCL
          ENDIF

          IF (ITER.GE.IMAX) THEN
            WRITE(6666,*) 'NUMERO DE ITERACOES MAIOR QUE O MAXIMO'
            WRITE(6,*) 'NUMERO DE ITERACOES MAIOR QUE O MAXIMO'
            STOP
          ENDIF
        END DO
!    FIM DO LA?O ITERATIVO (DO WHILE)

        ITERM=ITERM+ITER

!  APLICA??O DAS CONDI??ES DE CONTORNO ESSENCIAIS OU DE DIRICHILET
        KIK = 1
        cont    = cont + 1
        tnoRODA = tnoRODA + (omp_get_wtime() - ttick)
        ttick   = omp_get_wtime()      
        CALL RODA(V1,V2,HTOT,ICC,VX,VY,HHOT,HHTA, H, V1CO,V2CO,HC0,
     .            KIK, PP, ITYPBC, HMIN, NORMP, SNORMP,NNOS,NTCC,NTLC)
        tRODA  = tRODA + (omp_get_wtime() - ttick)
        ttick  = omp_get_wtime()                               
                                    

!    ARMAZENAMENTO DAS COMPONENTES DE VELOCIDADE E DA PROFUNDIDADE TOTAL

        if(m1.EQ.2)  THEN
          IT1=NTEMPO-ITEMPO
!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
          DO I=1,NNM
            GUARV1(I,IT1)=V1(I)
            GUARV2(I,IT1)=V2(I)
            GUARHT(I,IT1)=HTOT(I)
            GUARPP(I,IT1)= PP(I)
            GDM(I)=0.0D0
            GDF1(I)=0.0D0
            GDF2(I)=0.0D0
            GD12(I)=0.0D0
            GD21(I)=0.0D0
            GDPA1(I)=0.0D0
            GDPA2(I)=0.0D0
            GDH1(I)=0.0D0
            GDH2(I)=0.0D0
          END DO
!!$OMP END DO
!!$OMP END PARALLEL
        ELSE
!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
          DO I=1,NNM
            GDM(I)=0.0D0
            GDF1(I)=0.0D0
            GDF2(I)=0.0D0
            GD12(I)=0.0D0
            GD21(I)=0.0D0
            GDPA1(I)=0.0D0
            GDPA2(I)=0.0D0
            GDH1(I)=0.0D0
            GDH2(I)=0.0D0
          ENDDO
!!$OMP END DO
!!$OMP END PARALLEL
        ENDIF

!    CALCULO DA FUN??O DE FORMA E DE SUAS DERIVADAS. -
!    SUAVIZA??O DAS DERIVADAS DE ALGUMAS VARIVEIS
        DO NE=1,NEM
          CALL SELXY(NE,KONE,CORDX,CORDY,ELXY,NNOS,NELEM3)
          CALL ELEM(NE,KONE,V1,V2,PAT,H,ELV1,ELV2,ELPAT,ELH,0,
     .              NNOS,NELEM3)
          CALL SHAPET(ELXY,GDSF1,GDSF2,DET,B,D)
          CALL MASSTD(DET,CM,DM)
          CALL VETORT(GDSF1,GDSF2,DET,ELV1,ELV2,EDF1,EDF2,
     .                EDPA1,EDPA2,ELH,ED12,ED21,EDH1,EDH2,ELPAT)
          CALL ASSEM(NE,GDM,DM,KONE,NNOS,NELEM3)
          CALL ASSEM(NE,GDF1,EDF1,KONE,NNOS,NELEM3)
          CALL ASSEM(NE,GDF2,EDF2,KONE,NNOS,NELEM3)
          CALL ASSEM(NE,GD12,ED12,KONE,NNOS,NELEM3)
          CALL ASSEM(NE,GD21,ED21,KONE,NNOS,NELEM3)
          CALL ASSEM(NE,GDPA1,EDPA1,KONE,NNOS,NELEM3)
          CALL ASSEM(NE,GDPA2,EDPA2,KONE,NNOS,NELEM3)
          CALL ASSEM(NE,GDH1,EDH1,KONE,NNOS,NELEM3)
          CALL ASSEM(NE,GDH2,EDH2,KONE,NNOS,NELEM3)
        END DO

!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
        DO I=1,NNM
          GDF1(I)=GDF1(I)/GDM(I)
          GDF2(I)=GDF2(I)/GDM(I)
          GD12(I)=GD12(I)/GDM(I)
          GD21(I)=GD21(I)/GDM(I)
          GDPA1(I)=GDPA1(I)/GDM(I)
          GDPA2(I)=GDPA2(I)/GDM(I)
          GDH1(I)=GDH1(I)/GDM(I)
          GDH2(I)=GDH2(I)/GDM(I)
        ENDDO
!!$OMP END DO
!!$OMP END PARALLEL

        IF (NNR .EQ. (NIR+1-NNK)) THEN
          IF (NFILE.GE.90) THEN
            NFILE=10
          ENDIF

          NFILE=NFILE+1
          CALL REGIST(V1,V2,HTOT,H,NFILE,NNOS)
          NNR = 1

          IF (NNK.LE.1) THEN
            NNK=NNK+1
          ENDIF

          IF (NNK.EQ.2) THEN
            NNK=1
          ENDIF
        ELSE
          NNR=NNR+1
        ENDIF
      END DO


!  FIM DO PASSO DO TEMPO: AS VARIVEIS DO PROBLEMA DIRETO
!    NO TEMPO N+1 FORAM CALCULADAS
!***********************************************************************************************
      if(m1.EQ.2)THEN
!    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

!    PROGRAMA PRINCIPAL DO PROBLEMA ADJUNTO

!    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

!    RECUPERA??O DAS VARI?VEIS DO PROBLEMA DIRETO
!    INICIALIZO LAS VARIABLES EN EL TIEMPO FINAL CALCULADO

!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
        DO I=1,NNM
          V1(I)=GUARV1(I,1)
          V2(I)=GUARV2(I,1)
          HTOT(I)=GUARHT(I,1)
          PP(I) = GUARPP(I,1)
          GDM(I)= 0.D0
          GDF1(I) = 0.D0
          GDF2(I)= 0.D0
          GD12(I) = 0.D0
          GD21(I) = 0.D0
          GDHT1(I) = 0.D0
          GDHT2(I)= 0.D0
        END DO
!!$OMP END DO
!!$OMP END PARALLEL

        DT1=0.5D0*DT
        NIRNTR=NIR*NTR
        NTEMPO=NIRNTR+1

!   CALCULO DA FUN??O DE FORMA E DE SUAS DERIVADAS.
!   SUAVIZA??O DAS DERIVADAS DE ALGUMAS VARIAVEIS

        DO NE=1,NEM
          CALL SELXY(NE,KONE,CORDX,CORDY,ELXY,NNOS,NELEM3)
          CALL ELEM0(NE,KONE,PSIX,PSIY,ELV1,ELV2,NNOS,NELEM3)
          CALL ELEMH(NE,KONE,FFI,ELH,NNOS,NELEM3)
          CALL SHAPET(ELXY,GDSF1,GDSF2,DET,B,D)
          CALL MASSTD(DET,CM,DM)
          CALL VETOR0(GDSF1,GDSF2,DET,ELV1,ELV2,ELH,EDF1,EDF2,
     .                ED12,ED21,EDHT1,EDHT2)
          CALL ASSEM(NE,GDM,DM,KONE,NNOS,NELEM3)
          CALL ASSEM(NE,GDF1,EDF1,KONE,NNOS,NELEM3)
          CALL ASSEM(NE,GDF2,EDF2,KONE,NNOS,NELEM3)
          CALL ASSEM(NE,GD12,ED12,KONE,NNOS,NELEM3)
          CALL ASSEM(NE,GD21,ED21,KONE,NNOS,NELEM3)
          CALL ASSEM(NE,GDHT1,EDHT1,KONE,NNOS,NELEM3)
          CALL ASSEM(NE,GDHT2,EDHT2,KONE,NNOS,NELEM3)
        END DO

!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
        DO I=1,NNM
          GDF1(I)=GDF1(I)/GDM(I)
          GDF2(I)=GDF2(I)/GDM(I)
          GD12(I)=GD12(I)/GDM(I)
          GD21(I)=GD21(I)/GDM(I)
          GDHT1(I)=GDHT1(I)/GDM(I)
          GDHT2(I)=GDHT2(I)/GDM(I)
          GDH1(I)=GGDH1(I)
          GDH2(I)=GGDH2(I)
        ENDDO
!!$OMP END DO
!!$OMP END PARALLEL

!   INICIO DO PASSO DO TEMPO

        CTIME=0.0D0

        NNR = 1
        NNK = 0
        ITERM = 0

!cccc        CALCULO DT


        CALL CDT(CORDX,CORDY,KONE,V1,V2,HTOT,NNOS,NELEM3)


        DO ITEMPO = 1,NIRNTR
          CTIME=CTIME+DT
          CTIME1=NIRNTR*DT-CTIME

!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
          DO I=1,NNM
            GPR(I)=0.0D0
            GPV1(I)=0.0D0
            GPV2(I)=0.0D0
            GDM(I)=0.0D0
            GCR(I)=0.0D0
            GCV1(I)=0.0D0
            GCV2(I)=0.0D0
          ENDDO
!!$OMP END DO
!!$OMP END PARALLEL

          IF (CTIME.GT.1.E-10)THEN
!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
            DO I=1,NNM
              PSIX(I)=PSIXA(I)
              PSIY(I)=PSIYA(I)
              FFI(I)=FFIA(I)
            END DO
!!$OMP END DO
!!$OMP END PARALLEL

          END IF

!           CALCULO DAS VARIVEIS NO TEMPO N+1/2
          G=GRAV
          IT = ITEMPO+1
!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
          DO I =1, NNM
            VX(I)= GUARV1(I, IT)
            VY(I)= GUARV2(I, IT)
            HTOT(I)= GUARHT(I, IT)
            PP(I) = GUARPP(I, IT)
          END DO
!!$OMP END DO
!!$OMP END PARALLEL

          DO NE=1,NEM
            CALL SELXY(NE,KONE,CORDX,CORDY,ELXY,NNOS,NELEM3)
            CALL ELEM0(NE,KONE,V1,V2,ELV1,ELV2,NNOS,NELEM3)
            CALL ELEMH(NE,KONE,HTOT,ELH,NNOS,NELEM3)
            CALL ELEM0(NE,KONE,PSIX,PSIY,ELPSIX,ELPSIY,NNOS,NELEM3)
            CALL ELEMH(NE,KONE,FFI,ELFFI,NNOS,NELEM3)
            CALL ELEMH(NE,KONE,CHE,ELCHE,NNOS,NELEM3)
            CALL ELEMD(NE,KONE,GDF1,GDF2,GD12,GD21,EDF1,EDF2,ED12,ED21,
     .                 NNOS,NELEM3)

            CALL SHAPET(ELXY,GDSF1,GDSF2,DET,B,D)
            CALL CALFT0(NE,KONE,GDSF1,GDSF2,GGDH1,GGDH2,GDHT1,GDHT2,GDF1,
     .                  GDF2,GD12,GD21,H,ELCHE,ELV1,ELV2,ELH,ELF1,ELF2,
     .                  ELFFI,ELPSIX,ELPSIY,FFIEL,PSIXEL,PSIYEL,
     .                  NNOS,NELEM, NELEM3,G)

!      CALCULOS DAS VARIVEIS NO TEMPO N+1

!    RECUPERA??O DAS COMPONENTES DE VELOCIDADE E PROFUNDIDADE TOTAL
            NE1=3*(NE-1)
!  IT=ITEMPO+1
            DO I=1,3
              IND=KONE(NE1+I)
              ELV1(I)=GUARV1(IND,IT)
              ELV2(I)=GUARV2(IND,IT)
              ELH(I)=GUARHT(IND,IT)
            END DO
            CALL FLVT0(NE,KONE,FFIEL,PSIXEL,PSIYEL,GDSF1,GDSF2,H,ELCHE,
     .                 ELV1,ELV2,ELH,GGDH1,GGDH2,F11,F12,F13,F21,F22,F23,
     .                 NODO,HMAX,PPL1,PPL2,PPL3,NNOS,NELEM,NELEM3,G)


            CALL MASSTC(DET,CM,ELFFI,ELPSIX,ELPSIY,PCL1,PCL2,PCL3)
            CALL ASSEM(NE,GPR,PPL1,KONE,NNOS,NELEM3)
            CALL ASSEM(NE,GPV1,PPL2,KONE,NNOS,NELEM3)
            CALL ASSEM(NE,GPV2,PPL3,KONE,NNOS,NELEM3)
            CALL ASSEM(NE,GCR,PCL1,KONE,NNOS,NELEM3)
            CALL ASSEM(NE,GCV1,PCL2,KONE,NNOS,NELEM3)
            CALL ASSEM(NE,GCV2,PCL3,KONE,NNOS,NELEM3)

            DO K=1,3
              F11N(K)=ELF1(K,1)
              F12N(K)=ELF1(K,2)
              F13N(K)=ELF1(K,3)
              F21N(K)=ELF2(K,1)
              F22N(K)=ELF2(K,2)
              F23N(K)=ELF2(K,3)
            END DO

            CALL BOUFOR(NE,CORDX,CORDY,KONE,ISIDEC,GPR,GPV1,GPV2,
     .                  F11,F12,F13,F21,F22,F23,F11N,F12N,F13N,
     .                  F21N,F22N,F23N,SNORMC,NNOS,NELEM3,NTLC)
            CALL MASST(DET,CM,DM,SDM)
            CALL ASSEM(NE,GDM,DM,KONE,NNOS,NELEM3)
            CALL MONTA1(GSDM,SDM,NE1,NELEM3)
          END DO

          ITER=0
          TOL1 = 0.0D0
          TOL2=0.0D0

          CALL INICIOLO(FFI,PSIX,PSIY,PSIX00,PSIY00,FFI00,NNM,NNOS)

!    INICIO DO LA?O ITERATIVO


          DO WHILE (ITER.EQ.0.OR.(TOL1.GT.TOL.OR.TOL2.GT.TOL))
            SUM1=0.0D0
            SUM2=0.0D0
            SUM3=0.0D0
            SUM5=0.0D0
            SUM6=0.0D0
            SUM7=0.0D0

!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
            DO I=1,NNM
              GPRK(I)=0.0D0
              GPV1K(I)=0.0D0
              GPV2K(I)=0.0D0
            ENDDO
!!$OMP END DO
!!$OMP END PARALLEL

            ITER=ITER+1

            DO NE=1,NEM
              NE1=(NE-1)*3
              CALL MONTA2(GSDM,SDM,NE1,NELEM3)
              CALL ELEMK(NE,KONE,FFI00,PSIX00,PSIY00,EHTK,ELV1K,ELV2K,NNOS,
     .                   NELEM3)
              CALL DMLVTQ(EHTK,ELV1K,ELV2K,SDM,PLLL1,PLLL2,PLLL3)
              CALL ASSEM(NE,GPRK,PLLL1,KONE,NNOS,NELEM3)
              CALL ASSEM(NE,GPV1K,PLLL2,KONE,NNOS,NELEM3)
              CALL ASSEM(NE,GPV2K,PLLL3,KONE,NNOS,NELEM3)
            END DO

!ESTE METODO SE UTILIZA PARA CALCULAR LAS CONDICIONES DE BORDE PARA EL PROBLEMA ADJUNTOs
            CALL CONDI0(PSIX,PSIY,FFI,GPR,GPV1,GPV2,GDM,GPRK,GPV1K
     .                  ,GPV2K,GCR,GCV1,GCV2,PSIX00,PSIY00,FFI00,PSIXA,PSIYA,FFIA,
     .                  ITYPB0,ICC0,V1,V2,NTCC,
     .                  ITER,NNOS)

            IF (CC1.GT.1.E-06)THEN
              CALL CDT1(CORDX,CORDY,KONE,VX,VY,HTOT,DTE,NNOS,NELEM3,NELEM)

!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
              DO I=1,NNM
                GPDR(I)=0.0D0
                GPDV1(I)=0.0D0
                GPDV2(I)=0.0D0
                GSKEN(I)=0.0D0
                GSKED(I)=0.0D0
              END DO
!!$OMP END DO
!!$OMP END PARALLEL

              DO NE=1,NEM
                NE1=(NE-1)*3
                CALL MONTA2(GSDM,SDM,NE1,NELEM3)
                CALL ELEMP(NE,KONE,PP,ELP,PAT,NNOS,NELEM3)
                CALL SKELEM(SDM,ELP,SKEN,SKED)
                CALL ASSEM(NE,GSKEN,SKEN,KONE,NNOS,NELEM3)
                CALL ASSEM(NE,GSKED,SKED,KONE,NNOS,NELEM3)
              END DO

              CALL DIVI(GSKEN,GSKED,NNOS)

              DO NE=1,NEM
                CALL ELEMS(NE,KONE,GSKEN,SM,NNOS,NELEM3)

                NE1=(NE-1)*3
                CALL MONTA2(GSDM,SDM,NE1,NELEM3)

                CALL ELEM0(NE,KONE,PSIX00,PSIY00,ELV1K,ELV2K,NNOS,NELEM3)
                CALL ELEMH(NE,KONE,FFI00,EHTK,NNOS,NELEM3)
                CALL DMLCFL(ELV1K,ELV2K,EHTK,SDM,PLLS1,PLLS2,
     .                      PLLS3,DTE,SM,NE,CC1,NELEM)
                CALL ASSEM(NE,GPDV1,PLLS2,KONE,NNOS,NELEM3)
                CALL ASSEM(NE,GPDR,PLLS1,KONE,NNOS,NELEM3)
                CALL ASSEM(NE,GPDV2,PLLS3,KONE,NNOS,NELEM3)
              END DO


              FFIMIN=-10**6

              CALL SMOOTH(PSIX,PSIY,FFI,GPDR,GPDV1,GPDV2,GDM,
     .                    ITYPB0,FFIMIN,NNOS)
            END IF


!  FIM DO PROCESSO DE SUAVIZA??O
!  ANALISE DA CONVERG?NCIA DO PROCESSO ITERATIVO.
!    ***********************************************

            CALL CONVERG(PSIX00,PSIY00,FFI00,PSIXA,PSIYA,FFIA,SUM1,SUM2,
     .                   SUM3,SUM5,SUM6,SUM7,NNOS)

            DEUCL=DSQRT(SUM1+SUM2)
            EUCL=DSQRT(SUM5+SUM6)
            IF (DABS(EUCL).LT.0.1E-06) THEN
              TOL1=DEUCL
            ELSE
              TOL1 = DEUCL / EUCL
            ENDIF
            DEUCL=DSQRT(SUM3)
            EUCL = DSQRT(SUM7)
            IF (DABS(EUCL).LT.0.1E-06) THEN
              TOL2=DEUCL
            ELSE
              TOL2 = DEUCL / EUCL
            ENDIF
            IF (ITER.GE.IMAX) THEN
              WRITE(6,*) 'NUMERO DE ITERACOES MAIOR QUE O MAXIMO'
              STOP
            ENDIF
          END DO


!    FIM DO LA?O ITERATIVO
          ITERM=ITERM+ITER
!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
          DO I = 1, NNM
            PSIX(I)= PSIX00(I)
            PSIY(I)= PSIY00(I)
            FFI(I)= FFI00(I)
          END DO
!!$OMP END DO
!!$OMP END PARALLEL

!  APLICA??O DAS CONDI??ES DE CONTORNO ESSENCIAIS OU DE DIRICHILET
          KIK=1

          CALL RODA0(PSIX,PSIY,FFI,ICC0,V1,V2,PSIX0,PSIY0,FFI0,PSIXA,
     .               PSIYA,FFIA,KIK,ITYPB0,NORMP,SNORMP,NNOS,NTCC,NTLC, CTIME,G)

          IT=ITEMPO+1
!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
          DO I=1,NNM
!    CALCULO DA SENSIBILIDADE
            SENS(I)=FFI(I)+(V1(I)*PSIX(I)+V2(I)*PSIY(I))

!    RECUPERA??O DAS COMPONENTES DE VELOCIDADE E DA PROFUNDIDADE TOTAL
            V1(I)=GUARV1(I,IT)
            V2(I)=GUARV2(I,IT)
            HTOT(I)=GUARHT(I,IT)
            GDM(I)=0.0D0
            GDF1(I)=0.0D0
            GDF2(I)=0.0D0
            GD12(I)=0.0D0
            GD21(I)=0.0D0
            GDHT1(I)=0.0D0
            GDHT2(I)=0.0D0
          END DO
!!$OMP END DO
!!$OMP END PARALLEL

!    CALCULO DA FUN??O DE FORMA E DE SUAS DERIVADAS. -
!    SUAVIZA??O DAS DERIVADAS DE ALGUMAS VARIVEIS

          DO NE=1,NEM
            CALL SELXY(NE,KONE,CORDX,CORDY,ELXY,NNOS,NELEM3)
            CALL ELEM0(NE,KONE,V1,V2,ELV1,ELV2,NNOS,MELEM3)
            CALL ELEMH(NE,KONE,HTOT,ELH,NNOS,NELEM3)
            CALL SHAPET(ELXY,GDSF1,GDSF2,DET,B,D)
            CALL MASSTD(DET,CM,DM)
            CALL VETOR0(GDSF1,GDSF2,DET,ELV1,ELV2,ELH,EDF1,EDF2,
     .                  ED12,ED21,EDHT1,EDHT2)
            CALL ASSEM(NE,GDM,DM,KONE,NNOS,NELEM3)
            CALL ASSEM(NE,GDF1,EDF1,KONE,NNOS,NELEM3)
            CALL ASSEM(NE,GDF2,EDF2,KONE,NNOS,NELEM3)
            CALL ASSEM(NE,GD12,ED12,KONE,NNOS,NELEM3)
            CALL ASSEM(NE,GD21,ED21,KONE,NNOS,NELEM3)
            CALL ASSEM(NE,GDHT1,EDH1,KONE,NNOS,NELEM3)
            CALL ASSEM(NE,GDHT2,EDH2,KONE,NNOS,NELEM3)
          END DO

!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
          DO I =1,NNM
            GDF1(I)=GDF1(I)/GDM(I)
            GDF2(I)=GDF2(I)/GDM(I)
            GD12(I)=GD12(I)/GDM(I)
            GD21(I)=GD21(I)/GDM(I)
            GDHT1(I)=GDH1(I)/GDM(I)
            GDHT2(I)=GDH2(I)/GDM(I)
            PSIXA(I)= PSIX(I)
            PSIYA(I)= PSIY(I)
            FFIA(I)= FFI(I)
          ENDDO
!!$OMP END DO
!!$OMP END PARALLEL

          IF (NNR .EQ. (NIR+1-NNK)) THEN
            IF (NFILE.GE.90) NFILE=10

            NFILE=NFILE+1
            CALL REGIS0(PSIX,PSIY,FFI,SENS,CTIME1,NFILE,NNOS)
            NNR = 1
            IF (NNK.LE.1)     NNK=NNK+1
            IF (NNK.EQ.2)    NNK=1
          ELSE
            NNR=NNR+1
          END IF

        END DO

      END IF


!  FIM DO PASSO DO TEMPO: AS VARIVEIS NO TEMPO N+1 FORAM CALCULADAS
!**********************************************************************************************


      WRITE(6,3000) CTIME,DT,NIRNTR
      WRITE(6,*)
 3000 FORMAT(/10X,' TEMPO FINAL                      =',E12.5,
     .       /10X,' INTERVALO DE TEMPO                =',E12.5,
     .   /10X,' NUMERO TOTAL DE INTERVALOS DE TEMPO =',I14)

      finish_time = omp_get_wtime()

      CALL WRITE_TIME_MEASURE("tiempos.txt", start_time, finish_time,
     .                       FALSE)
      
      tnoRODA  = tnoRODA + (omp_get_wtime() - ttick)
      ttot     = ttot    + (omp_get_wtime() - tstart)
      write(1000,'('' MAIN PROFILING...'')')
      write(1000, *) cont
      write(1000,'('' RODA time     :'', f10.3)') tRODA
      write(1000,'('' Total MnLoop time:'', f10.3)') tTotalMainLoop
      write(1000,'('' No RODA time  :'', f10.3)') tnoRODA        
      write(1000,'('' Total time    :'', f10.3)') ttot

!    File for writing everything related to timing and profile
      CLOSE(1000)

      STOP
      END

!  END PROGRAM
!  FIM DO PROGRAMA PRINCIPAL
!  ESTA SUBRUTINA TIENE EL PROPOSITO DE ESCRIBIR LA MEDICION DE TIEMPO
!  CONSUMIDO DE CPU POR EL PROGRAMA CON PROPOSITOS DE PROFILING DE ALTO
!  NIVEL
      SUBROUTINE WRITE_TIME_MEASURE(FILENAME, START_TIME, END_TIME,
     .                              PRINT_DETAILS)
      CHARACTER*20 FILENAME
      REAL*8 START_TIME
      REAL*8 END_TIME
      LOGICAL PRINT_DETAILS

      LOGICAL exists_timefile
      REAL*8 total_time
      INTEGER fd_tiempos

!    ES EL IDENTIFICADOR DE ARCHIVO PARA tiempos.txt
!    EN ESTE ARCHIVO SE GUARDAN TODOS LOS TIEMPOS DE CADA EJECUCION
      fd_tiempos = 90

      inquire(file=FILENAME, exist=exists_timefile)

      IF(exists_timefile) THEN
        open(fd_tiempos, file=FILENAME, status="old",
     .           position="append", action="write")
      ELSE
        open(fd_tiempos, file=FILENAME,
     .           status="new", action="write")
      END IF

      total_time = END_TIME - START_TIME
      IF(print_details) THEN
      WRITE(fd_tiempos, 9997) START_TIME
 9997 FORMAT('TIEMPO INICIAL  =', E12.5)
      WRITE(fd_tiempos, 9998) END_TIME
 9998 FORMAT('TIEMPO FINAL    =', E12.5)
      END IF

      WRITE(fd_tiempos, 9999) total_time
 9999 FORMAT('TIEMPO TOTAL DE EJECUCION    =', E12.5)

      CLOSE(fd_tiempos)
      RETURN
      END

!***********************************************************************
      SUBROUTINE RDATA(KONE,CORDX,CORDY,V1,V2,H,HTOT,P,FFI,PSIX,PSIY,
     .                 ICC,ICC0,ISIDEC,NORMP,ITYPBC,ITYPB0,SNORMP,
     .                 SNORMC,CC,CC1,PAT,V1CO,V2CO,HC0,FFI0,PSIX0,PSIY0,
     .                 NNOS,NTCC,NTLC,NELEM3, SPART, PART)
!***********************************************************************
!                 LEITURA DE DADOS
! DESCRIPCION: LEER TODOS LOS DATOS NECESARIOS PARA LA EJECUCION DEL PROGRAMA PRESENTE
!          EN LOS DIFERENTES ARCHIVOS
!          TODOS LOS ARCHIVOS TIENEN UNA LONGITUD FIJA EN EL NOMBRE DE 6 CARACTERES
!          PTNAME VA A ALMACENAR EL NOMBRE DEL ARCHIVO A SER LEIDO EN CADA PASO
!
!          1. ASIGNAMOS EN PTNAME EL PREFIJO PARA EL NOMBRE DE LOS ARCHIVOS.
!          2. SE LEEN DIFERENTES VALORES DESDE EL ARCHIVO ENTRADA.CHU (CODIGO 5)
!             SE LEE EL VALOR PARA EL COEFICIENTE DE CORIOLIS (DEPENDE DE LAMBDA QUE ES LA LATITUD)
!                    F = 2 * (VEL ROTACION TERRESTRE)*SIN(LAMBDA)
!             ASIGNAMOS LAS TENSIONES DEBIDAS AL VIENTO [Peso/Sup] SEGUN LO INDICADA EN EL ARCHIVO
!              HTAU1 TENSION EN EJE X (EXPRESION MULTIPLICADA POR EL COS)
!                    HTAU2 TENSION EN EJE Y (EXPRESION MULTIPLICADA POR EL SIN)
C
!              HTAUi = COEF * (VELVIENTO AL CUADRADO) * (SIN O COS)(ANGULO)
!                 TAMBIEN SE LEEN DESDE EL ARCHIVO
!              RHOIN DENSIDAD
!              TOL   TOLERANCIA
!              IMAX  NUMERO MAXIMO DE ITERACIONES
!              CH    COEFICIENTE DE CHEZY
!              AMPL  AMPLITUD DE LA ONDA
!              PERI  PERIODO DE LA ONDA DE MAR
!              VMU   VISCOSIDAD DEL FLUIDO
!              CC    NUMERO DE COURANT SE USA PARA LA SUAVIZACION
!              CC1   NUMERO DE COURANT SE USA PARA LA SUAVIZACION
!              GRAV  ACELARACION TERRESTRE DE LA GRAVEDAD 9.81
!
!          3. SE LEEN LAS COORDENADAS DESDE EL ARCHIVO CHUCOR. (CODIGO 20)
!           ACTUALMENTE ESTE ARCHIVO ES SALIDA DEL ANSYS.
!           TIENE QUE TENER TANTOS VALORES COMO CANTIDAD DE NODOS DE LA MALLA
!                  COORDX - COORDENADAS DE CADA NODO EN EL EJE X
!                  COORDY - COORDENADAS DE CADA NODO EN EL EJE Y
!               CADA UNA DE LAS COORDENADAS SE MULTIPLICA POR 25 PARA AGRANDAR EL AREA
!           ANALIZADA POR LA MALLA (AUMENTA PROPORCIONALMENTE).
!          4. SE ESCRIBEN LOS VALORES DE MANERA COMPRENSIBLE EN SAIDA.CHU
!          5. ASIGNA COMO PRESION ATMOSFERICA (PAT) A CADA NODO UNA CONSTANTE 1.0
!                 ACTUALMENTE EL CODIGO PARA LA ASIGNACION DE LOS VALORES EN CADA PUNTO
!           ESTA COMENTADO.
!          6. LEEMOS LAS CONECTIVIDADES DE LOS DIFERENTES NODOS. ARCHIVO CHUCON (CODIGO 4) OVERWRITE
!           ESTO QUIERE DECIR QUE VA A TENER 3 VALORES REPRESENTANDO EL NUMERO DE NODO
!           INTERPRETANDOLOS EN CONJUNTO NOS DICE QUE EL ELEMENTO I ESTA CONFORMADO POR
!           LOS NODOS N1, N2 y N3 (POR EJEMPLO).
!           A LA MATRIZ DE CONECTIVIDADES LA REPRESENTA COMO UN ARRAY UNIDIMENSIONAL
!           EL ARRAY UNIDIMENSIONAL ES LA VARIABLE KONE.
!          7. LEEMOS LAS CONDICIONES INICIALES. CHU000 (CODIGO 10).
!           ACTUALMENTE NO SE ESTA USANDO EL ARCHIVO
!
!           ASIGNA EL TIEMPO INICIAL CTIME (EN NUESTRO CASO 0.0)
!           ASIGNA EL INCREMENTO DE TIEMPO DT (0.3)
!           ASIGNA LA ALTURA TOTAL Y ALTURA TOPOGRAFICA PARA CADA X.
!              H : Altura topografica
!              HTOT: Altura total
!           TAMBIEN ASIGNA LAS VELOCIDADES V1 (velocidad en x) y V2 (velocidad en y) PARA EL INSTANTE INCIAL.
!           COMO TRABAJAMOS CON EL PROBLEMA DEL LAGO QUIETO LA VELOCIDAD EN Y ES 0.0 MIENTRAS QUE EN X
!           ES MUY BAJA.
!            8. LEE CONDICIONES INICIALES DEL PROBLEMA ADJUNTO (* ACTUALMENTE NO LO USAMOS)
!          9. LEEMOS LOS VALORES DE LOS NODOS DE CONTORNO PARA EL PROBLEMA DIRECTO. ESTOS VALORES
!           SE LEEN DESDE EL ARCHIVO CHUDBD (CODIGO 7).
!               K ES EL NUMERO DE NODO DE CONTORNO DEL QUE SE ESTAN LEYENDO LOS DATOS
!               ITYPBC(K)=ITP     --- PREGUNTAR
!                   NORMP(I)=K        --- PREGUNTAR
!                   SNORMP(I)=ANGLE   CARGA LOS ANGULOS PARA CADA UNO DE LOS NODOS DE CONTORNO: SE USAN EN BOUFOR
!                           PARA CALCULAR LAS CIRCULACIONES.
!
!         10. LEEMOS LOS VALORES DE LOS NODOS DE CONTORNO PARA EL PROBLEMA ADJUNTO. (* ACTUALMENTE NO LO USAMOS)
!         11. LEEMOS LOS VALORES DE LOS LADOS DE CONTORNO PARA EL PROBLEMA DIRECTO. ARCHIVO CHUEBD (CODIGO 300)
!           PARA NSIDC (NUMERO DE LADOS DE CONTORNO). PREGUNTAR
!              LEE FIJARME EN CARPETA
!              ASIGNA EN ISIDEC(1,I), ISIDEC(2,I) Y EL ANGULO
!         12. LEE LAS CONDICIONES DE CONTORNO DE DIRICHLET. ARCHIVO CHU0BC (CODIGO 3000).
!              ICC NUMERO DE CONTORNO CON COND. DIRICHLET (PREGUNTAR).
!              V1CO PROYECCION DE VELOCIDAD EN DIRECCION X PARA EL NODO
!              V2CO PROYECCION DE VELOCIDAD EN DIRECCION Y PARA EL NODO
!              HC0 VALOR DE LA ALTURA
!          ACORDARSE QUE CUANDO TENEMOS CONDICIONES DE DIRICHLET LOS VALORES DEL CAMPO A CALCULAR SON
!          CONOCIDOS. EN NUESTRO CASO EL CAMPO ES (p,q,h) => (velX, velY, altura).
!         13. LEEMOS LOS VALORES PARA EL PROBLEMA ADJUNTO. (* ACTUALMENTE NO LO USAMOS)
!         14. LLAMAMOS A LA SUBRUTINA RODA
!              APLICA CONDICIONES DE CONTORNO
!         15. LLAMAMOS A LA SUBRUTIONA RODA0
!**********************************************************************

      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/TITULO/PNAME,PTNAME
      COMMON/IMP/NIR,NTR,IMAX
      COMMON/NELPA/NNM,NEM,NBC,NTBN,NSIDC,NEMNPE
      COMMON/TEMP/CTIME,DT,DT1
      COMMON/ESCFLU/TOL,VMU,RHOIN,F,CH,HTAU1,HTAU2,AMPL,PERI,GRAV
      COMMON/PARVAR/NPROC,NPART
      CHARACTER PNAME*3,PTNAME*6
      DIMENSION CORDX(NNOS),CORDY(NNOS),V1(NNOS),V2(NNOS),H(NNOS),
     .          V1H(NNOS),V2H(NNOS),HTOT(NNOS),P(NNOS),PAT(NNOS),
     .        PSIXA(NNOS),PSIYA(NNOS),ITYPBC(NNOS),ITYPB0(NNOS),
     .          FFI(NNOS),PSIX(NNOS),PSIY(NNOS),FFI0(NTCC),PSIX0(NTCC),
     .          PSIY0(NTCC),ICC0(NTCC),HHTA(NNOS),HHOT(NNOS),
     .          V1CO(NTCC),V2CO(NTCC),HC0(NTCC),ICC(NTCC),SNORMP(NTLC),
     .          SNORMC(NTLC),ISIDEC(2,NTLC),NORMP(NTLC),KONE(NELEM3),
     .          FFIA(NNOS)

!     INTEGER, dimension(:), allocatable :: SPART
!Valores para cada una de las particiones
!     type darray
!           real, dimension(:), allocatable :: values
!     end type darray
!     type(darray), dimension(:), allocatable :: PART

!    SETEAMOS EL PREFIJO DE LOS NOMBRES DE LOS ARCHIVOS A SER LEIDOS
        PTNAME(1:3)=PNAME

!  LAS TENSIONES DEL VIENTO ESTAN DADAS POR:
!  HTAU1=COEF*(VELOCIDADE DO VENTO**2)*COS(THETA)
!  HTAU2=COEF*(VELOCIDADE DO VENTO**2)*SIN(THETA).
!  "COEF" ? UNA CONSTANTE QUE VARIA ENTRE 3.0E-06 E 4.0E-06
!  "THETA" ? EL ANGULO QUE FORMA LA DIRECCION DEL VIENTO CON EL EJE "X"

!  EL COEFICIENTE DE CORIOLIS VIENE DADO POR:
!  F=2*(VELOCIDAD DE ROTACION TERRESTRE)*SIN(LAMBDA)=1.458E-04*SIN(LAMBDA)
!  "LAMBDA" ? LATITUDE

!  USAR COMO UNIDADES: NEWTONS, METROS, SEGUNDOS
      READ(5,*)RHOIN,TOL,IMAX,F,CH,HTAU1,HTAU2,AMPL,PERI,VMU,
     .  CC,CC1,GRAV



           WRITE(6,4000)RHOIN,TOL,IMAX,AMPL,F,CH,HTAU1,HTAU2,PERI,VMU,
     .CC,CC1,GRAV
 4000 FORMAT(10X,'RHOIN (densidad)   =',E13.6//,
     .            10X,'TOL(Tolerancia)   =',E13.6//,
     .            10X,'IMAX (NUMERO M?XIMO DE ITERA??ES )  =',I13//,
     .            10X,'AMPL(AMPLIFICA??O DA ONDA DE MAR?) =',E13.6//,
     .            10X,'F (COEFICIENTE DE CORI?LIS)         =',E13.6//,
     .            10X,'CH (COEFICIENTE DE CHEZY)           =',E13.6//,
     .            10X,'HTAU1(COMPONENTE X DA TENS?O DO VENTO)=',E13.6//,
     .            10X,'HTAU2(COMPONENTE Y DA TENS?O DO VENTO)=',E13.6//,
     .            10X,'PERIODO(PERIODO DA ONDA DA MAR?)      =',E13.6//,
     .            10X,'VMU (VISCOSIDADE DO FLUIDO)       =',E13.6//,
     .            10X,'CC (COEFICIENTE PARA A SUAVIZA??O)   =',E13.6//,
     .            10X,'CC1 (COEFICIENTE PARA A SUAVIZA??O)   =',E13.6//
     .            10X,'GRAV (ACELERA??O DA GRAVIDADE)    =',E13.6//)


!FILE COR  'COORDENADAS - PRIMEIRO REGISTRO = NUMERO TOTAL DE N?S'


      PTNAME(4:6)='corx'
!     OPEN (20,FILE=PTNAME,STATUS='OLD')
       OPEN (20,FILE=PTNAME)
!DEBERIAMOS CAMBIARLO PARA QUE DIRECTAMENTE LEA EL VALOR MULTIPLICADO
!QUE HAYA UNA OPERACION IO INHIBE QUE EL CICLO SE PARALELICE
      DO I=1,NNM
      READ(20,*) CORDX(I),CORDY(I)
      CORDX(I) = CORDX(I) * 25.0D0
      CORDY(I) = CORDY(I) * 25.0D0
!FILE PAT  'PRESS?O ATMOSFERICA  igual 1atm. no se lee archivo de dato, se coloca directo
      PAT(I)=1.0
      END DO
      CLOSE (20)

!FILE CON  'CONETIVIDAES - PRIMEIRO REGISTRO = NUMERO DE ELEMENTOS'


      PTNAME(4:6)='con'
      OPEN (4,FILE=PTNAME,STATUS='OLD')


      DO I=1,NEM
        II=(I-1)*3
        READ(4,*) KONE(II+1),KONE(II+2),KONE(II+3)

      END DO
      CLOSE (4)

!NFILE     'LEITURA DAS CONDICOES INICIAIS' introducido en forma directa sin usar la lectura de chu000
!                                           introducimos en forma directa CTIME Y DT

!    PROBLEMA DIRETO

      CTIME=0.0
      DT=0.000250

!ASIGNAMOS TODOS LOS VALORES PARA LAS PROYECCIONES DE LAS VELOCIDADES  EN DIRECCION A X E Y.
!COMO ASI TAMBIEN LOS VALORES DE ALTURA TOTAL (PROFUNDIDAD TOTAL) Y ALTURA TOPOGRAFICA.
!CONOCEMOS ESTOS VALORES AL INICIO PORQUE ANALIZAMOS EL PROBLEMA DEL LAGO QUIETO. SIN EMBARGO
!LUEGO REPRESENTARAN LAS INCOGNITAS QUE QUEREMOS AVERIGUAR.
!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
      DO I = 1,NNM
      H(I) = 1.990D0  + 0.00001D0 * CORDX(I)
      HTOT(I)= 1.990D0 + 0.00001D0 * CORDX(I)
      V1(I) = 0.7156/HTOT(I)
      V2(I) =0.0D0
      END DO
!!$OMP END DO
!!$OMP END PARALLEL

!    PROBLEMA ADJUNTO

      PTNAME(4:6)='001'
      OPEN (11,FILE=PTNAME,STATUS='OLD')
      DO I=1,NNM
      READ(11,*)FFI(I),PSIX(I),PSIY(I)
      END DO
      CLOSE (11)

!FILE DBD  'NOS DE CONTORNO'
!    PROBLEMA DIRETO

      PTNAME(4:6)='dbd'
      OPEN (7,FILE=PTNAME,STATUS='OLD')

!INICIALIZAMOS CON UN VALOR POR DEFECTO DE CERO
      DO I=1,NNM
      ITYPBC(I)=0
      END DO

      DO I=1,NTBN
      READ (7,*) K,ITP,ANGLE
         ITYPBC(K)=ITP
         NORMP(I)=K
      SNORMP(I)=ANGLE
        END DO
      CLOSE(7)

!    PROBLEMA ADJUNTO
!FILE EBD  'LADOS DE CONTORNO'

!     PTNAME (4:6)='dbe'
!     OPEN (70,FILE=PTNAME,STATUS='OLD')
!     DO I=1, NNM
!     ITYPB0(I)=0
!     END DO
!     DO I=1,NTBN
!     READ(70,*) K,ITP0
!     ITYPB0(K)=ITP0
!     END DO
!     CLOSE(70)

!FILE EBD  'LADOS DE CONTORNO'

      PTNAME(4:6)='ebd'
      OPEN (300,FILE=PTNAME,STATUS='OLD')

      DO I=1,NSIDC
      READ(300,*) ISIDEC(1,I),ISIDEC(2,I),SNORMC(I)
      END DO
      CLOSE (300)

!FILE 0BC  'CONDICOES DE CONTORNO DE DIRICHILET'

!    PROBLEMA DIRETO

      PTNAME(4:6) ='0bc'
      OPEN (3000,FILE=PTNAME,STATUS='OLD')
      DO I = 1, NTBN
      READ(3000, *) ICC(I), V1CO(I), V2CO(I), HC0(I)
      END DO
      CLOSE (3000)


!    PROBLEMA ADJUNTO

!leer condiciones de contorno solo para la adjunta
      KIK = 0

      IF(m1.eq.2) THEN
      PTNAME(4:6)='1bc'
      OPEN (40,FILE=PTNAME,STATUS='OLD')
      DO I=1,NTBN
      READ(40,*) ICC0(I),PSIX0(I),PSIY0(I),FFI0(I)

      END DO
      CLOSE(40)

!    Cargamos el archivo con los datos para cada particion
      IFD_PART = 50
      OPEN(IFD_PART,FILE='partitions2.txt',STATUS='OLD')
      READ(IFD_PART,*) NPROC, NPART
      ALLOCATE(SPART(NPART))
      ALLOCATE(PART(NPART))    
!     READ(IFD_PART,*) SPART

!     DO I=1,NPART
!       ALLOCATE(PART(I)%VALUES(SPART(I)))
!       READ(IFD_PART, *) PART(I)%VALUES
!     END DO

      CLOSE(IFD_PART)
      

      CALL RODA0(PSIX,PSIY,FFI,ICC0,V1,V2,PSIX0,PSIY0,FFI0,PSIXA,
     .    PSIYA,FFIA,KIK,ITYPB0,NORMP,SNORMP,NNOS,NTCC,NTLC,CTIME,
     .       G)

      ELSE
        CALL RODA(V1,V2,HTOT,ICC,V1H,V2H,HHOT,HHTA,H,V1CO,V2CO,HC0,
     .          KIK,P,ITYPBC,HMIN,NORMP,SNORMP,NNOS,NTCC,NTLC)

      ENDIF

      RETURN
      END


!**********************************************************************
      SUBROUTINE REGIST(V1,V2,HTOT,H,NFILE,NNOS)
!**********************************************************************
!                 REGISTRA VALORES DAS VARIAVEIS DIRETAS EM DISCO
!**********************************************************************


      IMPLICIT REAL*8 (A-H,O-Z)

      COMMON/TITULO/PNAME,PTNAME
      COMMON/NELPA/NNM,NEM,NBC,NTBN,NSIDC,NEMNPE
      COMMON/TEMP/CTIME,DT,DT1
      CHARACTER PNAME*3,PTNAME*6
      CHARACTER PLOTNAME*9
      CHARACTER TI*2
      DIMENSION X(NNOS),Y(NNOS),V1(NNOS),V2(NNOS),HTOT(NNOS),
     .          H(NNOS),KONE(15360)

!NFILE     'REGISTRA VARIAVEIS'

      WRITE(TI,'(I2)') NFILE
      PTNAME = PNAME // 'S' // TI
      PLOTNAME = PNAME // TI // '.plt'

      PRINT *, PTNAME
      PRINT *, PLOTNAME
      OPEN (NFILE,FILE=PTNAME)
      OPEN (NFILE+1,FILE=PLOTNAME)
      OPEN (NFILE+2,FILE=PNAME//'con',STATUS='OLD')
      OPEN (NFILE+3,FILE=PNAME//'cor',STATUS='OLD')
      WRITE(NFILE,6000) CTIME,DT
      WRITE(NFILE,7000)

!Imprimimos los resultados a los archivos plt
      WRITE(NFILE+1,*) 'variables = "x", "y", "u", "v", "htot", "h"'
      WRITE(NFILE+1,9000) nnm, nem

      DO I=1,NNM
        READ(NFILE+3,*) X(I),Y(I)
        WRITE(NFILE,8000)I,V1(I),V2(I),HTOT(I),H(I)
        WRITE(NFILE+1,*) x(I), y(I), v1(I)
        WRITE(NFILE+1,*) v2(I), htot(I), h(I)
!POWERSTATION FORTRAN IMPRIME CON ANCHO FIJO DE 80. PARA COMPARAR
!FACIL CONTRA LA SALIDA ESCRITA POR GFORTRAN LO DIVIDIMOS EN DOS FILAS
!SIMULANDO COMO ESCRIBIRIA UN PROGRAMA COMPILADO CON POWERSTATION
!       WRITE(NFILE+1,*) x(I), y(I), v1(I)
!       WRITE(NFILE+1,*) v2(I), htot(I), h(I)
      END DO

      CLOSE(NFILE+3)

!Imprimimos la topologia de los elementos
      DO I=1,NEM
        II=(I-1)*3
        READ(NFILE+2,*) KONE(II+1),KONE(II+2),KONE(II+3)
        WRITE(NFILE+1,*) kone(II+1), kone(II+2), kone(II+3)
      END DO

      CLOSE (NFILE+2)
      CLOSE (NFILE+1)

      CLOSE (NFILE)
 6000 FORMAT(5X,'TEMPO:0',F14.6,'PASSO DE TEMPO:',F14.6,//,
     .   5X,'V1,V2:COMPONETES DA VELOCIDADE',/,
     .   5X,'HT:ALTURA TOTAL',/,
     .   5X,'HB:ALTURA BARIMTRICA',//)
 7000 FORMAT(6X,'NO',8X,'V1',14X,'V2',14X,'HT',14X,'HB',/,
     .  6X,'==',8X,'==',3(14X,'=='),/)
 8000 FORMAT(3X,I5,4(2X,F14.6))

      WRITE(6,*) 'NFILE=',NFILE,' CTIME=',CTIME

 9000 FORMAT('zone n = ',i6,' e = ',i6, ' f = fepoint',' et = triangle')

      RETURN
      END


!**********************************************************************
      SUBROUTINE REGIS0(PSIX00,PSIY00,FFI00,SENS,CTIME1,NFILE,NNOS)
!**********************************************************************
!                 REGISTRA VALORES DAS VARIAVEIS ADJUNTAS EM DISCO
!**********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z)

      COMMON/TITULO/PNAME,PTNAME
      COMMON/NELPA/NNM,NEM,NBC,NTBN,NSIDC,NEMNPE
      COMMON/TEMP/CTIME,DT,DT1
      CHARACTER PNAME*3,PTNAME*6,PLOTNAME*9
      CHARACTER TI*2
      DIMENSION X(NNOS),Y(NNOS),SENS(NNOS),PSIX00(NNOS),
     .PSIY00(NNOS),FFI00(NNOS),KONE(15360)

!NFILE     'REGISTRA VARIAVEIS'

      WRITE(TI,'(I2)') NFILE
      PTNAME = PNAME // 'S' // TI
      PLOTNAME = PNAME // TI // '.plt'

      PRINT *, PTNAME

      PRINT *, PLOTNAME

      OPEN (NFILE,FILE=PTNAME)
      OPEN (NFILE+1,FILE=PLOTNAME)
      OPEN (NFILE+2,FILE=PNAME//'con',STATUS='OLD')
      OPEN (NFILE+3,FILE=PNAME//'cor',STATUS='OLD')
      WRITE(NFILE,6500) CTIME,DT,CTIME1

      WRITE(NFILE,7500)
      WRITE(NFILE+1,*) 'variables = "x", "y", "FFI00", "PSIX00",
     ."PSIY00", "SENS"'
      WRITE(NFILE+1,9000) nnm, nem

      DO I=1,NNM
      READ(NFILE+3,*) X(I),Y(I)
      WRITE(NFILE,8500)I,PSIX00(I),PSIY00(I),FFI00(I)
      WRITE(NFILE+1,*) x(I), y(I), FFI00(I), PSIX00(I), PSIY00(I),
     .  SENS(I)
      END DO
      CLOSE(NFILE+3)

      WRITE(NFILE,9500)

      DO I=1,NNM
      WRITE(NFILE,9600)I,SENS(I)
      END DO

      DO I=1,NEM
      II=(I-1)*3
      READ(NFILE+2,*) KONE(II+1),KONE(II+2),KONE(II+3)
      WRITE(NFILE+1,*) kone(II+1), kone(II+2), kone(II+3)
      END DO


      CLOSE (NFILE+1)
      CLOSE (NFILE+2)
      CLOSE (NFILE)
 6500 FORMAT(5X,'TEMPO:0',E14.6,'PASSO DE TEMPO:',E14.6,//,
     .      5X,'TEMPO "REAL" DO PROBLEMA ADJUNTO:',E14.6,//,
     .      5X,'PSIX,PSIY,FI: VARIVEIS ADJUNTAS',//)
     .
 7500 FORMAT(6X,'NO',7X,'PSIX',12X,'PSIY',13X,'FI',/,
     .      6X,'==',7X,'====',12X,'====',13X,'==',/)
 8500 FORMAT(3X,I5,3(2X,E14.6))
 9000 FORMAT('zone n = ',i6,' e = ',i6, ' f = fepoint',' et = triangle')
 9500 FORMAT(///,3X,'VALORES DA SENSIBILIDADE',/,3X,24('='),//,
     .        5X,'NO',5X,'SENSIBILIDADE',/,5X,'==',5X,13('='),/)

 9600 FORMAT(1X,I6,5X,E14.6)
      WRITE(6,*) 'NFILE=',NFILE,'            CTIME=',CTIME

      RETURN
      END



!**********************************************************************
      SUBROUTINE CDT(CORDX,CORDY,KONE,V1,V2,H,NNOS,NELEM3)
!**********************************************************************
!                 CALCULO DO INTERVALO DE TEMPO
!**********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z)

      COMMON/NELPA/NNM,NEM,NBC,NTBN,NSIDC,NEMNPE
      COMMON/TEMP/CTIME,DT,DT1
      COMMON/ESCFLU/TOL,VMU,RHOIN,F,CH,HTAU1,HTAU2,AMPL,PERI,GRAV

      DIMENSION CORDX(NNOS),CORDY(NNOS),V1(NNOS),V2(NNOS),H(NNOS),
     .          KONE(NELEM3)
      DIST(XI,YI,XF,YF)=DSQRT((XF-XI)*(XF-XI)+(YF-YI)*(YF-YI))

      DTMIN=DT

      DO I=1,NEM
        IND=(I-1)*3
        N1=KONE(IND+1)
        N2=KONE(IND+2)
        N3=KONE(IND+3)
        X1=CORDX(N1)
        Y1=CORDY(N1)
        X2=CORDX(N2)
        Y2=CORDY(N2)
        X3=CORDX(N3)
        Y3=CORDY(N3)
        DM1=DIST(X1,Y1,X3,Y3)
        DM2=DIST(X2,Y2,X1,Y1)
        DM3=DIST(X3,Y3,X2,Y2)
        SS=(DM1+DM2+DM3)/2.0D0
        AA=DSQRT(SS*(SS-DM1)*(SS-DM2)*(SS-DM3))
        AA=AA*2
        DH1=AA/DM1
        DH2=AA/DM2
        DH3=AA/DM3
        DX=DMIN1(DH1,DH2,DH3)
        H1=H(N1)
        H2=H(N2)
        H3=H(N3)
        HM=DMAX1(H1,H2,H3)
        VM1=DSQRT(V1(N1)*V1(N1)+V2(N1)*V2(N1))
        VM2=DSQRT(V1(N2)*V1(N2)+V2(N2)*V2(N2))
        VM3=DSQRT(V1(N3)*V1(N3)+V2(N3)*V2(N3))
        V=DMAX1(VM1,VM2,VM3)
        DTP=0.5773*DX/(DSQRT(GRAV*HM)+V)
        IF (DTP.LT.DTMIN) DTMIN=DTP
        END DO

      DT=DTMIN

      RETURN
      END

!**********************************************************************
      SUBROUTINE CDT1(CORDX,CORDY,KONE,V1,V2,H,DTE,NNOS,NELEM3,NELEM)
!**********************************************************************
!                 CALCULO DO INTERVALO DE TEMPO LOCAL
!**********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z)
      COMMON/NELPA/NNM,NEM,NBC,NTBN,NSIDC,NEMNPE
      COMMON/ESCFLU/TOL,VMU,RHOIN,F,CH,HTAU1,HTAU2,AMPL,PERI,GRAV
      DIMENSION CORDX(NNOS),CORDY(NNOS),V1(NNOS),V2(NNOS),
     .          H(NNOS),DTE(NELEM),KONE(NELEM3)
      DIST(XI,YI,XF,YF)=DSQRT((XF-XI)*(XF-XI)+(YF-YI)*(YF-YI))

      DO I=1,NEM
        IND=(I-1)*3
        N1=KONE(IND+1)
        N2=KONE(IND+2)
        N3=KONE(IND+3)
        X1=CORDX(N1)
        Y1=CORDY(N1)
        X2=CORDX(N2)
        Y2=CORDY(N2)
        X3=CORDX(N3)
        Y3=CORDY(N3)
        DM1=DIST(X1,Y1,X3,Y3)
        DM2=DIST(X2,Y2,X1,Y1)
        DM3=DIST(X3,Y3,X2,Y2)
        SS=(DM1+DM2+DM3)/2.
        AA=DSQRT(SS*(SS-DM1)*(SS-DM2)*(SS-DM3))
        AA=AA*2
        DH1=AA/DM1
        DH2=AA/DM2
        DH3=AA/DM3
        DX=DMIN1(DH1,DH2,DH3)
        H1=H(N1)
        H2=H(N2)
        H3=H(N3)
        HM=DMAX1(H1,H2,H3)
        IF(DABS(V1(N1)).LE.1.0E-06) V1(N1)=0.0D0
        IF(DABS(V1(N2)).LE.1.0E-06) V1(N2)=0.0D0
        IF(DABS(V1(N3)).LE.1.0E-06) V1(N3)=0.0D0
        IF(DABS(V2(N1)).LE.1.0E-06) V2(N1)=0.0D0
        IF(DABS(V2(N2)).LE.1.0E-06) V2(N2)=0.0D0
        IF(DABS(V2(N3)).LE.1.0E-06) V2(N3)=0.0D0
        VM1=DSQRT(V1(N1)*V1(N1)+V2(N1)*V2(N1))
        VM2=DSQRT(V1(N2)*V1(N2)+V2(N2)*V2(N2))
        VM3=DSQRT(V1(N3)*V1(N3)+V2(N3)*V2(N3))
        V=DMAX1(VM1,VM2,VM3)
        DTP=DX/(DSQRT(GRAV*HM)+V)
        DTE(I)=DTP
      END DO

      RETURN
      END

!**********************************************************************
      SUBROUTINE MOVIM(V1,V2,HTOT,V1H,V2H,NNOS)
!**********************************************************************
!                 CALCULA O VETOR QUANTIDADE DE MOVIMENTO
!**********************************************************************

      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/NELPA/NNM,NEM,NBC,NTBN,NSIDC,NEMNPE
      DIMENSION V1(NNOS),V2(NNOS),HTOT(NNOS),V1H(NNOS),V2H(NNOS)

!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
      DO I=1,NNM
      V1H(I)=V1(I)*HTOT(I)
      V2H(I)=V2(I)*HTOT(I)
      END DO
!!$OMP END DO
!!$OMP END PARALLEL

      RETURN
      END

!**********************************************************************
      SUBROUTINE SELXY(NE,KONE,CORDX,CORDY,ELXY,NNOS,NELEM3)
!**********************************************************************
!           CALCULA O VETOR DAS COORDENADAS GLOBAIS A N?VEL DO ELEMENTO
!**********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION CORDX(NNOS),CORDY(NNOS),ELXY(3,2), KONE(NELEM3)

      IK=3*(NE-1)
      DO I=1,3
      IND=KONE(IK+I)
      ELXY(I,1)=CORDX(IND)
      ELXY(I,2)=CORDY(IND)
      END DO
      RETURN
      END

!**********************************************************************
      SUBROUTINE ELEMH(NE,KONE,H,ELH,NNOS,NELEM3)
!**********************************************************************
!            CALCULA OS VETORES DA BATIMETRIA A N?VEL DO ELEMENTO
!**********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION H(NNOS),ELH(3), KONE(NELEM3)
      IK=3*(NE-1)

      DO I=1,3
      IND=KONE(IK+I)
      ELH(I)=H(IND)
      END DO

      RETURN
      END

!**********************************************************************
      SUBROUTINE ELEM(NE,KONE,V1,V2,P,HTOT,ELV1,ELV2,ELP,EHTOT,K,
     .              NNOS,NELEM3)
!**********************************************************************
!           CALCULA OS VETORES DAS VELOCIDADES, PRESSO E ALTURA
!                         TOTAL NO ELEMENTO
!**********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION V1(NNOS),V2(NNOS),P(NNOS),HTOT(NNOS),ELV1(3),ELV2(3),
     .          ELP(3),EHTOT(3),KONE(NELEM3)

      IK=3*(NE-1)
      DO I=1,3
      IND=KONE(IK+I)
      ELV1(I)=V1(IND)
      ELV2(I)=V2(IND)
      END DO

      IF (K.EQ.0) THEN
      DO I=1,3
      IND=KONE(IK+I)
      ELP(I)=P(IND)
      EHTOT(I)=HTOT(IND)
      END DO
      ENDIF

      RETURN
      END



!**********************************************************************
      SUBROUTINE ELEM0(NE,KONE,V1,V2,ELV1,ELV2,NNOS,NELEM3)
!**********************************************************************
!           CALCULA OS VETORES DAS VELOCIDADES, PRESSO E ALTURA
!                      TOTAL NO ELEMENTO
!**********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION V1(NNOS),V2(NNOS),ELV1(3),ELV2(3),KONE(NELEM3)

      IK=3*(NE-1)
      DO I=1,3
      IND=KONE(IK+I)
      ELV1(I)=V1(IND)
      ELV2(I)=V2(IND)
      END DO

      RETURN
      END

!**********************************************************************
      SUBROUTINE ELEMD(NE,KONE,GDF1,GDF2,GD12,GD21,
     .                 EDF1,EDF2,ED12,ED21,NNOS,NELEM3)
!**********************************************************************
!           CALCULA OS VETORES DAS DERIVADAS DAS VELOCIDADES
!                        DO ELEMENTO
!**********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION GDF1(NNOS),GDF2(NNOS),GD12(NNOS),GD21(NNOS),
     .          EDF1(3),EDF2(3),ED12(3),ED21(3),
     .          KONE(NELEM3)

      IK=3*(NE-1)
      DO I=1,3
      IND=KONE(IK+I)
      EDF1(I)=GDF1(IND)
      EDF2(I)=GDF2(IND)
      ED12(I)=GD12(IND)
      ED21(I)=GD21(IND)
      END DO

      RETURN
      END

!**********************************************************************
      SUBROUTINE SHAPET(ELXY,GDSF1,GDSF2,DET,B,D)
!**********************************************************************
!    CALCULA AS DERIVADAS DA FUN??O DE FORMA PARA O ELEMENTO TRIANGULAR
!    LINEAR,  COM RELA??O ?S COORDENADAS GLOBAIS E A AREA DO ELEMENTO
!                            (DET/2)
!**********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION ELXY(3,2),GDSF1(3),GDSF2(3),B(3),D(3)

      B(1)=ELXY(2,2)-ELXY(3,2)
      B(2)=ELXY(3,2)-ELXY(1,2)
      B(3)=ELXY(1,2)-ELXY(2,2)
      D(1)=ELXY(3,1)-ELXY(2,1)
      D(2)=ELXY(1,1)-ELXY(3,1)
      D(3)=ELXY(2,1)-ELXY(1,1)
      DET=B(1)*D(2)-B(2)*D(1)
      IF (DET.LE.0.D0) THEN
        DO I=1,3
                DO J=1,2
                        WRITE(6666,*) (ELXY(I,J))
                ENDDO
        ENDDO
        STOP 'ERRO-AREA NULA OU NEGATIVA'
      ENDIF
!    DERIVADAS

      DO J=1,3
         GDSF1(J)=B(J)/DET
         GDSF2(J)=D(J)/DET
        END DO

      RETURN
      END

!**********************************************************************
      SUBROUTINE MASST(DET,CM,DM,SDM)
!**********************************************************************
!    CALCULA AS MATRIZES MASSA CONSISTENTE CM, A MASSA CONCENTRADA DM
!    E  A  DIFEREN?A ENTRE AMBAS SDM, PARA O ELEMENTO TRIANGULAR.
!**********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION CM(3,3),DM(3),SDM(3,3)

      AUXC=DET/12.0D0
      AUXD=DET/6.0D0

      DO J=1,3
      CM(J,J)=AUXC
      DM(J)=AUXD
      END DO

      AUXC=AUXC/2.0D0
      AUXS=AUXC

      DO I=1,2
      II=I+1
      DO J=II,3
        CM(I,J)=AUXC
        CM(J,I)=AUXC
        SDM(I,J)=AUXS
        SDM(J,I)=AUXS
      ENDDO
      END DO

      DO I=1,3
      SDM(I,I)=CM(I,I)-DM(I)
      END DO

      RETURN
      END

!**********************************************************************
      SUBROUTINE MASSTD(DET,CM,DM)
!**********************************************************************
!    CALCULA AS MATRIZES MASSA CONSISTENTE CM, A MASSA CONCENTRADA DM
!    PARA SEREM USADAS NO CALCULO DAS  DERIVADAS DOS N?S PARA O
!                   ELEMENTO TRIANGULAR
!**********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION CM(3,3),DM(3)

      AUXC=DET/12.0D0

      DO J=1,3
      CM(J,J)=AUXC
      DM(J)=DET/6.0D0
      END DO

      AUXC=AUXC/2.0D0

      DO I=1,2
      II=I+1
      DO J=II,3
        CM(I,J)=AUXC
        CM(J,I)=AUXC
      END DO
      END DO

      RETURN
      END

!**********************************************************************
      SUBROUTINE MASSTC(DET,CM,EHTOT,ELV1H,ELV2H,PCL1,PCL2,PCL3)
!**********************************************************************
!    CALCULA AS MATRIZES MASSA CONSISTENTE CM E MULTIPLICA PELO VETOR
!    DAS VARIAVEIS U NO TEMPO T, PARA O ELEMENTO TRIANGULAR
!**********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION CM(3,3),EHTOT(3),ELV1H(3),ELV2H(3),PCL1(3),PCL2(3),
          PCL3(3)

      AUXC=DET/12.0D0

      DO J=1,3
      CM(J,J)=AUXC
      END DO

      AUXC=AUXC/2.0D0

      DO I=1,2
      II=I+1
      DO J=II,3
        CM(I,J)=AUXC
        CM(J,I)=AUXC
      END DO
      END DO

      DO I=1,3
      PCL1(I)=0.0D0
      PCL2(I)=0.0D0
      PCL3(I)=0.0D0

      DO J=1,3
        PCL1(I)=PCL1(I)+CM(I,J)*EHTOT(J)
        PCL2(I)=PCL2(I)+CM(I,J)*ELV1H(J)
        PCL3(I)=PCL3(I)+CM(I,J)*ELV2H(J)
      END DO
      END DO

      RETURN
      END

!**********************************************************************
      SUBROUTINE MONTA1(GSDM,SDM,NE1,NELEM3)
!**********************************************************************
!      MONTA A MATRIZ DIFER?NCIA DE MASSAS CONSISTENTE E DISCRETA.
!**********************************************************************

      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GSDM(NELEM3,3),SDM(3,3)

      DO I=1,3
      DO J=1,3
        K=NE1+I
        GSDM(K,J)=SDM(I,J)
      END DO
      END DO

      RETURN
      END

!**********************************************************************
      SUBROUTINE MONTA2(GSDM,SDM,NE1,NELEM3)
!**********************************************************************
!       DESMONTA A MATRIZ DIFER?NCIA DE MASSAS CONSISTENTE E DISCRETA.
!**********************************************************************

      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GSDM(NELEM3,3),SDM(3,3)

      DO I=1,3
      DO J=1,3
        K=NE1+I
        SDM(I,J)=GSDM(K,J)
      END DO
      END DO

      RETURN
      END

!**********************************************************************
      SUBROUTINE INICIOLO(HTOT,V1H,V2H,VX,VY,HHOT,NNM,NNOS)
!**********************************************************************
!         INICIALIZA AS VARIAVEIS EM T PARA O LOOP ITERATIVO
!**********************************************************************

      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION HTOT(NNOS),V1H(NNOS),V2H(NNOS),VX(NNOS),
          HHOT(NNOS),VY(NNOS)

!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
      DO I=1,NNM
      HHOT(I)=HTOT(I)
      VX(I)=V1H(I)
      VY(I)=V2H(I)
      END DO
!!$OMP END DO
!!$OMP END PARALLEL

      RETURN
      END

!**********************************************************************
      SUBROUTINE ASSEM(NE,GV,V,KONE,NNOS,NELEM3)
!**********************************************************************
!           COLOCA O VETOR 'V' DO ELEMENTO NO VETOR GLOBAL 'GV'
!**********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION GV(NNOS),V(3),KONE(NELEM3)

      IK=3*(NE-1)
      DO I=1,3
        NR=KONE(IK+I)
        GV(NR)=GV(NR)+V(I)
      END DO

      RETURN
      END

!**********************************************************************
      SUBROUTINE CALFT(GDSF1,GDSF2,DET,ELV1,ELV2,ELP,EHTOT,ELV1H,
                 ELV2H,F11,F12,F13,F21,F22,F23,ELV11P,ELV22P,
                 ELV1V2,ELV2V1,EDF1,EDF2,ED12,ED21,U1,U2,U3,
                 U5,U6,U7,FONT1,FONT2,FONT3,EDPA1,EDPA2,EDH1,
                 EDH2,FNTM1,FNTM2,FNTM3,ELH,ELCHE)
!**********************************************************************
!        CALCULA OS VETORES   U  E  F  EM 'T+DT/2' PARA O  ELEMENTO
!                      TRIANGULAR LINEAR
!**********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z)
      COMMON/TEMP/CTIME,DT,DT1
      COMMON/ESCFLU/TOL,VMU,RHOIN,F,CH,HTAU1,HTAU2,AMPL,PERI,GRAV
      DIMENSION GDSF1(3),GDSF2(3),ELV1(3),ELV2(3),ELP(3),EHTOT(3),
          ELV1H(3),ELV2H(3),ELV11P(3),ELV22P(3),ELV2V1(3),
          ELV1V2(3),EDF1(3),EDF2(3),ED12(3),ED21(3),
          EDPA1(3),EDPA2(3),EDH1(3),EDH2(3),FONT1(3),
          FONT2(3),FONT3(3),ELH(3),ELCHE(3)

      AREA=0.5D0*DET
      AREA1=AREA/3.0D0

!    CALCULO DE F EM 'T'

      DO I=1,3

       ELV11P(I)= ELV1(I)*ELV1H(I)+ELP(I)+
              ((2.*VMU*EHTOT(I))/(3.*RHOIN))*EDF1(I)
       ELV22P(I)= ELV2(I)*ELV2H(I)+ELP(I)
              +((2.*VMU*EHTOT(I))/(3.*RHOIN))*EDF2(I)
       ELV1V2(I)= ELV1(I)*ELV2H(I)
              -(VMU*EHTOT(I)/RHOIN)*(ED21(I)+ED12(I))
       ELV2V1(I)= ELV2(I)*ELV1H(I)
              -(VMU*EHTOT(I)/RHOIN)*(ED21(I)+ED12(I))

       IF(DABS(CH).LE.1.0E-06)THEN
       FONT1(I)=0.0
       FONT2(I)= F*EHTOT(I)*ELV2(I)+GRAV*(EHTOT(I)-ELH(I))*EDH1(I)
                -(EHTOT(I)/RHOIN)*EDPA1(I)+HTAU1/RHOIN

       FONT3(I)= -F*EHTOT(I)*ELV1(I)+GRAV*(EHTOT(I)-ELH(I))*EDH2(I)
             -(EHTOT(I)/RHOIN)*EDPA2(I)+HTAU2/RHOIN
       ELSE
       FONT1(I)=0.0
       FONT2(I)=F*EHTOT(I)*ELV2(I)+GRAV*(EHTOT(I)-ELH(I))*EDH1(I)
          -GRAV*DSQRT(ELV1(I)*ELV1(I)+ELV2(I)*ELV2(I))*ELV1(I)/
           (ELCHE(I)*ELCHE(I))-(EHTOT(I)/RHOIN)*EDPA1(I)
           +HTAU1/RHOIN
       FONT3(I)=-F*EHTOT(I)*ELV1(I)+GRAV*(EHTOT(I)-ELH(I))*EDH2(I)
          -GRAV*DSQRT(ELV2(I)*ELV2(I)+ELV1(I)*ELV1(I))*ELV2(I)/
       (ELCHE(I)*ELCHE(I))-(EHTOT(I)/RHOIN)*EDPA2(I)+HTAU2/RHOIN
       ENDIF
      END DO

!    CLCULO DE U EM 'T+DT/2'

      A1=(EHTOT(1)+EHTOT(2)+EHTOT(3))*AREA1
      A2=(ELV1H(1)+ELV1H(2)+ELV1H(3))*AREA1
      A3=(ELV2H(1)+ELV2H(2)+ELV2H(3))*AREA1


!    CLCULO DA INTEGRAL RELATIVA AO VETOR FONTE*DT1

      FT1=(FONT1(1)+FONT1(2)+FONT1(3))*AREA1*DT1
      FT2=(FONT2(1)+FONT2(2)+FONT2(3))*AREA1*DT1
      FT3=(FONT3(1)+FONT3(2)+FONT3(3))*AREA1*DT1
      DA1=0.0D0
      DA2=0.0D0
      DA3=0.0D0
      DA5=0.0D0
      DA6=0.0D0
      DA7=0.0D0

      DO I=1,3
      DA1=DA1 + ELV1H(I)*GDSF1(I)
      DA2=DA2 + ELV11P(I)*GDSF1(I)
      DA3=DA3 + ELV1V2(I)*GDSF1(I)
      DA5=DA5 + ELV2H(I)*GDSF2(I)
      DA6=DA6 + ELV2V1(I)*GDSF2(I)
      DA7=DA7 + ELV22P(I)*GDSF2(I)
      END DO

      DA1=DA1*DT1*AREA
      DA2=DA2*DT1*AREA
      DA3=DA3*DT1*AREA
      DA5=DA5*DT1*AREA
      DA6=DA6*DT1*AREA
      DA7=DA7*DT1*AREA

!    CLCULO DA ALTURA MEDIA H NO ELEMENTO

      H1=ELH(1)
      H2=ELH(2)
      H3=ELH(3)
      HM=(H1+H2+H3)/3.0D0

!    CLCULO DO U MEDIO NO ELEMENTO EM T+1/2

      U1=(A1-(DA1+DA5)+FT1)/AREA
      U2=(A2-(DA2+DA6)+FT2)/AREA
      U3=(A3-(DA3+DA7)+FT3)/AREA

      IF(DABS(U1).LE.1.0E-06) THEN
       U6=0.0D0
       U7=0.0D0
      ELSE
      U6=U2/U1
      U7=U3/U1
      ENDIF

      U5=0.5D0*GRAV*(U1*U1-HM*HM)

!    CLCULO DO GRADIENTE DE H  E PA MEDIO NO ELEMENTO EM 'T+DT/2'

      FNH1M=0.0D0
      FNH2M=0.0D0
      FNP1M=0.0D0
      FNP2M=0.0D0
      CHM=0.0D0

      DO I=1,3
      FNH1M=FNH1M+GRAV*(U1-HM)*EDH1(I)
      FNH2M=FNH2M+GRAV*(U1-HM)*EDH2(I)
      FNP1M=FNP1M+(U1/RHOIN)*EDPA1(I)
      FNP2M=FNP2M+(U1/RHOIN)*EDPA2(I)
      CHM=CHM+ELCHE(I)
      END DO

      FNH1M=FNH1M/3.0D0
      FNH2M=FNH2M/3.0D0
      FNP1M=FNP1M/3.0D0
      FNP2M=FNP2M/3.0D0
      CHM=CHM/3.0D0

!    CLCULO DE F EM 'T+DT/2'

      F11=U2
      F12=U2*U6+U5
      F13=U3*U6
      F21=U3
      F22=U2*U7
      F23=U3*U7+U5

!    CLCULO DO VETOR FONTE EM T+1/2

      IF(DABS(CH).LE.1.0E-06)THEN
      FNTM1=0.0
      FNTM2=F*U3+HTAU1/RHOIN+FNH1M-FNP1M
      FNTM3=-F*U2+HTAU2/RHOIN+FNH2M-FNP2M
      ELSE
      FNTM1=0.0
      FNTM2=F*U3+HTAU1/RHOIN-GRAV*DSQRT(U6*U6+U7*U7)*U6/(CHM*CHM)
                                            +FNH1M-FNP1M
      FNTM3=-F*U2+HTAU2/RHOIN-GRAV*DSQRT(U6*U6+U7*U7)*U7/(CHM*CHM)
                                            +FNH2M-FNP2M
      ENDIF
      RETURN
      END

!**********************************************************************
      SUBROUTINE FLVT(F11,F12,F13,F21,F22,F23,GDSF1,GDSF2,FNTM1,
                FNTM2,FNTM3,PPL1,PPL2,PPL3,DET)
!**********************************************************************
!          CALCULA O VETOR DE CARGAS DO ELEMENTO TRIANGULAR LINEAR
!**********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION GDSF1(3),GDSF2(3),PPL1(3),PPL2(3),PPL3(3)

      AREA=0.5D0*DET
      AREA1=AREA/3.D0

      DO I=1,3
        PPL1(I)=(GDSF1(I)*F11+GDSF2(I)*F21)*AREA+FNTM1*AREA1
        PPL2(I)=(GDSF1(I)*F12+GDSF2(I)*F22)*AREA+FNTM2*AREA1
        PPL3(I)=(GDSF1(I)*F13+GDSF2(I)*F23)*AREA+FNTM3*AREA1
      END DO

      RETURN
      END

!**********************************************************************
      SUBROUTINE BOUFOR(NE,CORDX,CORDY,KONE,ISIDEC,GPR,GPV1,GPV2,
                  F11,F12,F13,F21,F22,F23,F11N,F12N,F13N,
                  F21N,F22N,F23N,SNORMC,NNOS,NELEM3,NTLC)
!**********************************************************************
!    CALCULA AS FORCAS DE CONTORNO E ADICIONA AS MESMAS AOS VETORES
!                         GPH,GPV1,GPV2
!**********************************************************************

      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/NELPA/NNM,NEM,NBC,NTBN,NSIDC,NEMNPE

      DIMENSION CORDX(NNOS),CORDY(NNOS),SNORMC(NTLC),GPV1(NNOS),
          GPV2(NNOS),GPR(NNOS),F11N(3),F12N(3),F13N(3),
          F21N(3),F22N(3),F23N(3),
          KONE(NELEM3),ISIDEC(2,NTLC)

      K=(NE-1)*3
      NA=0
      NB=0
      NODE1=0
      NODE2=0
      DX=0.0D0
      DY=0.0D0
      DX1=0.0D0
      DY1=0.0D0


      DO I=1,NSIDC
        IF (NE.EQ.ISIDEC(1,I)) THEN
        NI=K+ISIDEC(2,I)
        NA=ISIDEC(2,I)
        IF (ISIDEC(2,I).EQ.3) THEN
          NI1=NI-3+1
          NB=NA-3+1
           ELSE
          NI1=NI+1
          NB=NA+1
        ENDIF
        NODE1=KONE(NI)
        NODE2=KONE(NI1)
        DX1=CORDX(NODE1)-CORDX(NODE2)
        DY1=CORDY(NODE2)-CORDY(NODE1)
        DD=DSQRT(DX1*DX1+DY1*DY1)
        ANG=SNORMC(I)
        DX=DD*DSIN(ANG)
        DY=DD*DCOS(ANG)
        F11M=0.0D0
        F12M=0.0D0
        F13M=0.0D0
        F21M=0.0D0
        F22M=0.0D0
        F23M=0.0D0

        DO J=1,3
           F11M=F11M+F11N(J)
           F12M=F12M+F12N(J)
           F13M=F13M+F13N(J)
           F21M=F21M+F21N(J)
           F22M=F22M+F22N(J)
           F23M=F23M+F23N(J)
        END DO

        F11M=F11M/3.0D0
        F12M=F12M/3.0D0
        F13M=F13M/3.0D0
        F21M=F21M/3.0D0
        F22M=F22M/3.0D0
        F23M=F23M/3.0D0

        PLC1A=((2*F11N(NA)+F11N(NB))*DY+(2*F21N(NA)+F21N(NB))*DX)
                                                      /6.0D0
        PLC1B=((F11N(NA)+2*F11N(NB))*DY+(F21N(NA)+2*F21N(NB))*DX)
                                                      /6.0D0
        PLC2A=((2*F12N(NA)+F12N(NB))*DY+(2*F22N(NA)+F22N(NB))*DX)
                                                      /6.0D0
        PLC2B=((F12N(NA)+2*F12N(NB))*DY+(F22N(NA)+2*F22N(NB))*DX)
                                                      /6.0D0
        PLC3A=((2*F13N(NA)+F13N(NB))*DY+(2*F23N(NA)+F23N(NB))*DX)
                                                      /6.0D0
        PLC3B=((F13N(NA)+2*F13N(NB))*DY+(F23N(NA)+2*F23N(NB))*DX)
                                                      /6.0D0

        PLC1=((F11-F11M)*DY+(F21-F21M)*DX)*0.5D0
        PLC2=((F12-F12M)*DY+(F22-F22M)*DX)*0.5D0
        PLC3=((F13-F13M)*DY+(F23-F23M)*DX)*0.5D0
        GPR(NODE1)=GPR(NODE1)-PLC1-PLC1A
        GPR(NODE2)=GPR(NODE2)-PLC1-PLC1B
        GPV1(NODE1)=GPV1(NODE1)-PLC2-PLC2A
        GPV1(NODE2)=GPV1(NODE2)-PLC2-PLC2B
        GPV2(NODE1)=GPV2(NODE1)-PLC3-PLC3A
        GPV2(NODE2)=GPV2(NODE2)-PLC3-PLC3B
       ENDIF

      END DO

       RETURN
       END


!**********************************************************************
      SUBROUTINE ELEMK(NE,KONE,HTOT,V1,V2,EHTOT,ELV1,ELV2,NNOS,NELEM3)

!**********************************************************************
!    CALCULA OS VETORES, NO TEMPO T DAS VELOCIDADES E PROFUNDIDADE
!                      TOTAL DO ELEMENTO.
!**********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION V1(NNOS),V2(NNOS),HTOT(NNOS),ELV1(3),ELV2(3),EHTOT(3),
          KONE(NELEM3)

      IK=3*(NE-1)
      DO I=1,3
      IND=KONE(IK+I)
      ELV1(I)=V1(IND)
      ELV2(I)=V2(IND)
      EHTOT(I)=HTOT(IND)
      END DO

      RETURN
      END

!**********************************************************************
      SUBROUTINE DMLVTQ(EHTK,ELV1K,ELV2K,SDMDT,PLLL1,PLLL2,PLLL3)
!**********************************************************************
!    ESTA ROTINA CALCULA OS VETORES NA ITERACAO K  EM CADA EQUA??O
!    (PLL1,PLL2,PLL3) ORIGINADOS PELAS VARI?VEIS NO PROCESSO ITERATIVO
!**********************************************************************
      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION ELV1K(3),ELV2K(3),SDMDT(3,3),EHTK(3),PLLL1(3),
          PLLL2(3),PLLL3(3)

      DO I=1,3
      PLLL1(I)=0.0D0
      PLLL2(I)=0.0D0
      PLLL3(I)=0.0D0

      DO J=1,3
        PLLL1(I)=PLLL1(I)-SDMDT(I,J)*EHTK(J)
        PLLL2(I)=PLLL2(I)-SDMDT(I,J)*ELV1K(J)
        PLLL3(I)=PLLL3(I)-SDMDT(I,J)*ELV2K(J)

      END DO
      END DO

      RETURN
      END

!**********************************************************************
      SUBROUTINE CONDICA(V1H,V2H,HTOT,GPR,GPV1,GPV2,GDM,GPRK,GPV1K,
                   GPV2K,GCR,GCV1,GCV2,VX,VY,PP,HHOT,VXA,VYA,
                   HHTA,ITYPBC,H,ITER,HMIN,NNOS)
!**********************************************************************
!                 CALCULO DOS INCREMENTOS NO INTERVALO DT
!**********************************************************************
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/NELPA/NNM,NEM,NBC,NTBN,NSIDC,NEMNPE
      COMMON/TEMP/CTIME,DT,DT1
      COMMON/ESCFLU/TOL,VMU,RHOIN,F,CH,HTAU1,HTAU2,AMPL,PERI,GRAV
      DIMENSION H(NNOS),V1H(NNOS),V2H(NNOS),HTOT(NNOS),HHOT(NNOS),
          GPV1(NNOS),GPV2(NNOS),GPR(NNOS),GDM(NNOS),GPV1K(NNOS),
          GPV2K(NNOS),GPRK(NNOS),VX(NNOS),VY(NNOS),PP(NNOS),
          VXA(NNOS),VYA(NNOS),HHTA(NNOS),GCR(NNOS),GCV1(NNOS),
          GCV2(NNOS),ITYPBC(NNOS)

      PI=4.0D0 * DATAN(1.0D0)

!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
      DO I = 1, NNM
      IF (ITER.EQ.1) THEN
        VXA(I)=V1H(I)
        VYA(I)=V2H(I)
        HHTA(I)=HTOT(I)
      ELSE
        VXA(I)=VX(I)
        VYA(I)=VY(I)
        HHTA(I)=HHOT(I)
      ENDIF

      VX(I)=(GCV1(I)+GPV1(I)*DT+GPV1K(I))/GDM(I)
      VY(I)=(GCV2(I)+GPV2(I)*DT+GPV2K(I))/GDM(I)
      HHOT(I)=(GCR(I)+GPR(I)*DT+GPRK(I))/GDM(I)

      IF (ITYPBC(I).EQ.4. OR.ITYPBC(I).EQ.14.OR.ITYPBC(I).EQ.24.OR.
      ITYPBC(I).EQ.124) THEN
            HHOT(I)=HTOT(I)
      ENDIF

        IF (ITYPBC(I).EQ.3. OR.ITYPBC(I).EQ.13.OR.ITYPBC(I).EQ.23.OR.
      ITYPBC(I).EQ.123) THEN

           ARG=2.0D0 * PI * CTIME / PERI
           ETA=(AMPL/2.0D0)* DSIN(ARG)
           HHOT(I)= H(I) + ETA
      ENDIF

      IF (ITYPBC(I).EQ.1.OR.ITYPBC(I).EQ.12.OR.ITYPBC(I).EQ.13.OR.
    ITYPBC(I).EQ.14.OR.ITYPBC(I).EQ.123.OR.
      ITYPBC(I).EQ.124) THEN
        VX(I)=V1H(I)
      ENDIF

      IF (ITYPBC(I).EQ.2.OR.ITYPBC(I).EQ.12.OR.ITYPBC(I).EQ.23.OR.
    ITYPBC(I).EQ.24.OR.ITYPBC(I).EQ.123.OR.
      ITYPBC(I).EQ.124) THEN
        VY(I)=V2H(I)
      ENDIF

      IF(HHOT(I).LE.HMIN) THEN
        VX(I)=0.0D0
        VY(I)=0.0D0
      ENDIF
      PP(I)=0.5*GRAV*(HHOT(I)*HHOT(I)-H(I)*H(I))
      END DO
!!$OMP END DO
!!$OMP END PARALLEL

      RETURN
      END




!**********************************************************************
      SUBROUTINE CONDI0(V1H,V2H,HTOT,GPR,GPV1,GPV2,GDM,GPRK,GPV1K,
                  GPV2K,GCR,GCV1,GCV2,VX,VY,HHTOT,VXA,VYA,
                  HHTA,ITYPBC,ICC,
                  V1,V2,NTCC,ITER,NNOS)
!**********************************************************************
!                 CALCULO DOS INCREMENTOS NO INTERVALO DT
!**********************************************************************
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/NELPA/NNM,NEM,NBC,NTBN,NSIDC,NEMNPE
      COMMON/TEMP/CTIME,DT,DT1
      DIMENSION V1H(NNOS),V2H(NNOS),HTOT(NNOS),HHTOT(NNOS),GDM(NNOS),
          GPV1(NNOS),GPV2(NNOS),GPR(NNOS),V1(NNOS),V2(NNOS),
          GPV1K(NNOS),GPV2K(NNOS),GPRK(NNOS),VX(NNOS),VY(NNOS),
          VXA(NNOS),VYA(NNOS),HHTA(NNOS),GCR(NNOS),GCV1(NNOS),
          GCV2(NNOS),ITYPBC(NNOS),ICC(NTCC)


      IF (ITER.EQ.1) THEN
!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
      DO I=1,NNM
        VXA(I)=V1H(I)
        VYA(I)=V2H(I)
        HHTA(I)=HTOT(I)
      END DO
!!$OMP END DO
!!$OMP END PARALLEL

      ELSE
!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
      DO I=1,NNM
        VXA(I)=VX(I)
        VYA(I)=VY(I)
        HHTA(I)=HHTOT(I)
      END DO
!!$OMP END DO
!!$OMP END PARALLEL
      ENDIF



!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
       DO I=1,NNM
      VX(I)=(GCV1(I)+GPV1(I)+GPV1K(I))/GDM(I)
      VY(I)=(GCV2(I)+GPV2(I)+GPV2K(I))/GDM(I)
      HHTOT(I)=(GCR(I)+GPR(I)+GPRK(I))/GDM(I)
      END DO
!!$OMP END DO
!!$OMP END PARALLEL

!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
        DO I = 1, NNM
      IF (ITYPBC(I).EQ.1.OR.ITYPBC(I).EQ.12.OR.ITYPBC(I).EQ.13.OR.
  ITYPBC(I).EQ.14.OR.ITYPBC(I).EQ.123.OR.ITYPBC(I).EQ.124)
  VX(I)=V1H(I)

      IF (ITYPBC(I).EQ.2.OR.ITYPBC(I).EQ.12.OR.ITYPBC(I).EQ.23.OR.
  ITYPBC(I).EQ.24.OR.ITYPBC(I).EQ.123.OR.ITYPBC(I).EQ.124)
VY(I)=V2H(I)
      END DO
!!$OMP END DO
!!$OMP END PARALLEL

!!$OMP PARALLEL PRIVATE(I,TID) !NUM_THREADS(4)
!!$OMP DO SCHEDULE(DYNAMIC)
      DO I=1,NNM
      IF(ITYPBC(I).EQ.4.OR.ITYPBC(I).EQ.14.OR.ITYPBC(I).EQ.24.OR.
   ITYPBC(I).EQ.124) HHTOT(I)=HTOT(I)

      DO J=1,NTBN
      IF (ITYPBC(I).EQ.3.OR.ITYPBC(I).EQ.13.OR.ITYPBC(I).EQ.23.OR.
    ITYPBC(I).EQ.123) THEN
        IC=ICC(J)
        IF (IC.EQ.I) THEN
        HHTOT(IC)=-2.0D0*(V1(IC)*VX(IC)+V2(IC)*VY(IC))
      END IF
      END IF
        END DO
       END DO
!!$OMP END DO
!!$OMP END PARALLEL
      RETURN
      END


!**********************************************************************
      SUBROUTINE CONVERG(VX,VY,HHOT,VXA,VYA,HHTA,SUM1,SUM2,SUM3,
                   SUM5,SUM6,SUM7,NNOS)


!**********************************************************************
!                 CALCULO DA CONVERGENCIA
!**********************************************************************
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/NELPA/NNM,NEM,NBC,NTBN,NSIDC,NEMNPE
      DIMENSION VX(NNOS),VY(NNOS),HHOT(NNOS),VXA(NNOS),
          VYA(NNOS),HHTA(NNOS)

          DO I=1,NNM
            IF(DABS(VX(I)-VXA(I)).GT.1.0E-10) THEN
                SUM1=SUM1+(VX(I)-VXA(I))*(VX(I)-VXA(I))
            ENDIF

            IF(DABS(VY(I)-VYA(I)).GT.1.0E-10) THEN
                SUM2=SUM2+(VY(I)-VYA(I))*(VY(I)-VYA(I))
            ENDIF

            SUM3=SUM3+(HHOT(I)-HHTA(I))*(HHOT(I)-HHTA(I))

            IF(DABS(VX(I)).GT.1.0E-10) THEN
                SUM5=SUM5+VX(I)*VX(I)
            ENDIF

            IF(DABS(VY(I)).GT.1.0E-10) THEN
                SUM6=SUM6+VY(I)*VY(I)
            ENDIF

            SUM7=SUM7+HHOT(I)*HHOT(I)
        END DO
      RETURN
      END

!==============================================================================

      SUBROUTINE RODA(V1,V2,HTOT,ICC,VX,VY,HHOT,HHTA,H, V1CO,V2CO,HC0,
  KIK, P,ITYPBC, HMIN, NORMP, SNORMP, NNOS, NTCC,NTLC)
!**********************************************************************
!                 APLICA AS CONDICOES DE CONTORNO
!**********************************************************************

      IMPLICIT REAL*8(A-H,O-Z)

      COMMON/NELPA/NNM,NEM,NBC,NTBN,NSIDC,NEMNPE
!   COMMON/NELPA/NNM,NEM,NTBN,NSIDC

      COMMON/TEMP/CTIME,DT,DT1
      COMMON/ESCFLU/TOL,VMU,RHOIN,F,CH,HTAU1,HTAU2,AMPL,PERI,GRAV
      DIMENSION V1(NNOS),V2(NNOS),HHOT(NNOS),   P(NNOS),
          HHTA(NNOS),HTOT(NNOS),VX(NNOS),VY(NNOS),VN(NNOS),
          V1CO(NTCC),V2CO(NTCC), HC0(NTCC), H(NNOS), VT(NNOS),
          ICC(NTCC),ITYPBC(NNOS), NORMP(NTLC), SNORMP(NTLC)

      REAL*8 HGRAV

!    Intra-profile instrumentation and output...
      DOUBLE PRECISION tstart
      DOUBLE PRECISION tDOstart

      
      INTEGER, SAVE :: icont = 1
      INTEGER, SAVE :: iper  = 2000

      DOUBLE PRECISION, SAVE :: ttot   = 0.0 
      DOUBLE PRECISION, SAVE :: t1stDO = 0.0
      DOUBLE PRECISION, SAVE :: t2ndDO = 0.0
      DOUBLE PRECISION, SAVE :: t3rdDO = 0.0
      DOUBLE PRECISION, SAVE :: t4thDO = 0.0
      DOUBLE PRECISION, SAVE :: t5thDO = 0.0
      DOUBLE PRECISION, SAVE :: t6thDO = 0.0




      ! Initial tick
      tstart = omp_get_wtime()

      IF(KIK.NE. 0) THEN
        DO I=1,NNM
          HHTA(I)=HTOT(I)
          HTOT(I)=HHOT(I)
          IF(HTOT(I).LE.HMIN) THEN
            V1(I)=0.0D0
            V2(I)=0.0D0
          ELSE
            V1(I)=VX(I)/HTOT(I)
            V2(I)=VY(I)/HTOT(I)
          END IF
        END DO
      END IF
      
!    APLICACAO DAS CONDICOES DE CONTORNO DIRICHLET
      PI    = 4.0D0 * DATAN(1.0D0)
      ARG   = 2.0D0 * PI * CTIME /PERI
      ETA   = (AMPL/2.0D0)*DSIN(ARG)
      HGRAV = 0.5*GRAV


!    1st. nested DOs. Problem: ICC content...
      tDOstart = omp_get_wtime()
!$OMP PARALLEL DO PRIVATE(J, I) SHARED(NNM, NTBN, ICC, ITYPBC, HTOT, HC0, H, ETA, P, HGRAV)
      DO J = 1, NNM
        DO  I=1,NTBN
          IF(ICC(I). EQ. J) THEN
  
            IF(ITYPBC(J).EQ.4.OR. ITYPBC(J).EQ.14.OR. ITYPBC(J).EQ.24.
      OR. ITYPBC(J). EQ.124) HTOT(ICC(I))=  HC0(I)
     
            IF(ITYPBC(J). EQ.3.OR. ITYPBC(J).EQ.13.OR. ITYPBC(J).EQ.23.
      OR. ITYPBC(J).EQ.123) THEN
              HTOT(ICC(I))=H(ICC(I))+ETA
            END IF
          END IF
        END DO
        
!      0.5*GRAV...        
        P(J) = HGRAV*(HTOT(J)*HTOT(J)-H(J)*H(J))
      END DO
!$OMP END PARALLEL DO
      t1stDO = t1stDO + (omp_get_wtime() - tDOstart)

      ALFA = 0.03D0 * (CTIME-120.0D0)


!    2nd. nested DOs. Problem: none
      tDOstart = omp_get_wtime()
!$OMP PARALLEL DO PRIVATE(I, J, ANG, C, S) SHARED(NNM, NTBN, NORMP, SNORMP, VN, V1, V2, VT)
      DO I = 1, NNM
        DO J = 1, NTBN
          IF(NORMP(J). EQ. I) THEN
            ANG = SNORMP(J)
            C = DCOS(ANG)
            S = DSIN(ANG)

            VN(I)=V1(I) * C + V2(I) * S
            VT(I)=-V1(I) * S + V2(I) * C
c
!           IF(CTIME. LT. 160.0D0) VN(20) = 0.0D0
c
          END IF
        END DO
      END DO
!$OMP END PARALLEL DO
      t2ndDO = t2ndDO + (omp_get_wtime() - tDOstart)
      
      
!    Originally at the deepest level of the previous nested DOs...
      IF(CTIME. LT. 160.0D0) VN(20) = 0.0D0


!    3rd. nested DOs. Problem: ICC content...
      tDOstart = omp_get_wtime() 
!$OMP PARALLEL DO PRIVATE(J, I) SHARED(NNM, NTBN, ICC, ITYPBC, VN, V1CO, ALFA, HTOT)
      DO J = 1, NNM
        DO I = 1, NTBN
           IF(ICC(I). EQ. J) THEN
             IF(ITYPBC(J). EQ. 1. OR. ITYPBC(J).EQ. 12. OR. ITYPBC(J).EQ.13. OR. ITYPBC(J). EQ. 14.OR. ITYPBC(J). EQ. 123. OR.
       ITYPBC(J). EQ.124) THEN
               VN(ICC(I))= V1CO(I)
               IF(abs(V1CO(I)).GT.0.001)THEN
                 VN(ICC(I))= V1CO(I) - 4.0D0/
           ((EXP(-ALFA)+EXP(ALFA))*(EXP(-ALFA)+EXP(ALFA)))

                 VN(ICC(I))= VN(ICC(I))/ HTOT(ICC(I))
               END IF
             END IF
           END IF
         END DO
      END DO
!$OMP END PARALLEL DO
      t3rdDO = t3rdDO + (omp_get_wtime() - tDOstart)
      

!    4th. nested Dos. Problem: IC!content... 
      tDOstart = omp_get_wtime()
!$OMP PARALLEL DO PRIVATE(J, I, eps) SHARED(NNM, NTBN, ICC, ITYPBC, VT, V2CO, ALFA, HTOT)
      DO J =1, NNM
        DO I =1, NTBN
          IF(ICC(I).EQ.J) THEN
            IF(ITYPBC(J). EQ. 2. OR. ITYPBC(J).EQ. 12.OR. ITYPBC(J).EQ. 23.
      OR. ITYPBC(J).EQ. 24.OR. ITYPBC(J). EQ. 123.OR.ITYPBC(J).EQ.124) THEN
!    .      THEN
              VT(ICC(I))= V2CO(I)
              IF(abs(V2CO(I)).GT.0.001)THEN
                IF(ICC(I).EQ.1) THEN
                  eps = -1.0D0
                ELSE
!        IF(ICC(I).EQ.2) eps =  1.0D0
                  eps =  1.0D0
                END IF
                VT(ICC(I))= V2CO(I) + (eps * 4.0D0)/
          ((EXP(-ALFA)+EXP(ALFA))*(EXP(-ALFA)+EXP(ALFA)))
                VT(ICC(I))= VT(ICC(I))/ HTOT(ICC(I))
              END IF
            END IF
          END IF
        END DO
      END DO
!$OMP END PARALLEL DO
      t4thDO = t4thDO + (omp_get_wtime() - tDOstart)


!    5th. nested DOs. Problem: None
      tDOstart = omp_get_wtime()
!$OMP PARALLEL DO PRIVATE(I1, J, ANG, C, S) SHARED(NNM, NTBN, NORMP, SNORMP, V1, VN, VT, V2)
      DO I1 = 1, NNM
        DO J = 1, NTBN
          IF (NORMP(J) .EQ. I1) THEN
            ANG = SNORMP(J)
            C = DCOS(ANG)
            S = DSIN(ANG)
            V1(I1) =  VN(I1) * c - VT(I1) * s
            V2(I1) =  VN(I1) * s + VT(I1) * c
          END IF
        END DO
      END DO
!$OMP END PARALLEL DO      
      t5thDO = t5thDO + (omp_get_wtime() - tDOstart)


!    6th nested DOs. Problem: none
      tDOstart = omp_get_wtime()
!$OMP PARALLEL DO PRIVATE(II) SHARED(NNM, VX, HTOT, V1, VY, V2)
      DO II=1,NNM
        VX(II)=HTOT(II)*V1(II)
        VY(II)=HTOT(II)*V2(II)
      END DO
!$OMP END PARALLEL DO
      t6thDO = t6thDO + (omp_get_wtime() - tDOstart)



      ttot = ttot + (omp_get_wtime() - tstart)


      icont = icont + 1
      
      IF (MOD(icont,iper) == 0) THEN
        write(1000,'('' ***************'')')
        write(1000, *) icont, iper, MOD(icont,iper)
        write(1000,'('' Total time        :'', f10.3)') ttot
        write(1000,'('' 1st DO time       :'', f10.3)') t1stDO
        write(1000,'('' 2nd DO time       :'', f10.3)') t2ndDO
        write(1000,'('' 3rd DO time       :'', f10.3)') t3rdDO
        write(1000,'('' 4th DO time       :'', f10.3)') t4thDO
        write(1000,'('' 5th DO time       :'', f10.3)') t5thDO
        write(1000,'('' 6th DO time       :'', f10.3)') t6thDO
      END IF

      RETURN
      END SUBROUTINE RODA


!===============================================================================


!**********************************************************************
      SUBROUTINE RODA0(V1,V2,HTOT,ICC,VX,VY,V1CO,V2CO,HC0,V1A,V2A,
   HHTA,KIK,ITYPBC,NORMP,SNORMP,NNOS,NTCC,NTLC, CTIME,G)
!**********************************************************************
!                 APLICA AS CONDI??ES DE CONTORNO
!**********************************************************************
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/NELPA/NNM,NEM,NBC,NTBN,NSIDC,NEMNPE
      DIMENSION V1(NNOS),V2(NNOS),HTOT(NNOS),VX(NNOS),VY(NNOS),
          VN(NNOS),VT(NNOS),ITYPBC(NNOS),V1A(NNOS),V2A(NNOS),
          HHTA(NNOS),V1CO(NTCC),V2CO(NTCC),HC0(NTCC),
          ICC(NTCC),NORMP(NTLC),SNORMP(NTLC)


!  write(*,*)CTIME


        IF (KIK.EQ.1) THEN
      DO I=1,NNM
      V1A(I)=V1(I)
      V2A(I)=V2(I)
      HHTA(I)=HTOT(I)
      END DO
      END IF


!    APLICACAO DAS CONDICOES DE CONTORNO DIRECHLET


      DO I = 1, NNM
      DO J = 1, NTBN
      IF(NORMP(J). EQ. I) THEN
      ANG = SNORMP(J)
      C= DCOS(ANG)
      S= DSIN(ANG)
      VN(I)=V1(I) * C + V2(I) * S
      VT(I)=-V1(I) * S + V2(I) * C
      if(CTIME.LT.160.0D0) VN(20)=0.0D0
      END IF
      END DO
      END DO

      DO J = 1, NNM
      DO I = 1, NTBN
      IF(ICC(I). EQ. J) THEN
      IF(ITYPBC(J).EQ.1.OR.ITYPBC(J).EQ.12.OR.ITYPBC(J).EQ. 13.
OR.ITYPBC(J).EQ.14.OR.ITYPBC(J).EQ.123.OR.ITYPBC(J).EQ.124)
     .VN(ICC(I))=V1CO(I)






      IF(ITYPBC(J). EQ. 2. OR. ITYPBC(J).EQ. 12.OR. ITYPBC(J).EQ. 23.
     .OR. ITYPBC(J).EQ. 24.OR. ITYPBC(J). EQ. 123.OR.ITYPBC(J).EQ.124)
     .VT(ICC(I))= V2CO(I)
      END IF
      END DO
      END DO

!VERIFICAR ERRORES DE PROGRAMACION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      IF(CTIME.GT.120.0D0)THEN
      DO J = 1, NNM
      DO I = 1, NTBN
      IF(ICC(I). EQ. J) THEN
      IF(ITYPBC(J).EQ.1)VT(ICC(I))= 0.0
      IF(ITYPBC(J).EQ.1)HTOT(ICC(I))=-2.0D0*
     .  DSQRT(DABS(G*HTOT(ICC(I))))*V1(ICC(I))
         END IF
         END DO
         END DO
         END IF




      DO I1 = 1, NNM
      DO J = 1, NTBN
      IF (NORMP(J) .EQ. I1) THEN
      ANG = SNORMP(J)
      C = DCOS(ANG)
      S = DSIN(ANG)
      V1(I1) =  VN(I1) * C - VT(I1) * S
      V2(I1) =  VN(I1) * S + VT(I1) * C
      END IF
      END DO
      END DO

      DO J = 1, NNM
      IF(ITYPBC(J).EQ.4.OR.ITYPBC(J).EQ.14.OR.ITYPBC(J).EQ.24.
     .OR.ITYPBC(J).EQ.124)HTOT(ICC(I))=HC0(I)
      DO I=1,NTBN
      IF(ICC(I).EQ.J) THEN
      IF(ITYPBC(J).EQ.3.OR.ITYPBC(J).EQ.13.OR.ITYPBC(J).EQ.23.
     .OR.ITYPBC(J).EQ.123) THEN
      IC=ICC(I)
      HTOT(IC)=-2.0D0*(VX(IC)*V1(IC)+VY(IC)*V2(IC))
      ENDIF
      ENDIF
      END DO
      END DO

      RETURN
      END

!**********************************************************************
      SUBROUTINE VETOR0(GDSF1,GDSF2,DET,ELV1,ELV2,ELH,EDF1,EDF2,ED12,
     .                  ED21,EDHT1,EDHT2)

!**********************************************************************
!    ESTA ROTINA FAZ A SUAVIZA??O GLOBAL DAS DERIVADAS DA VELOCIDADE,
!    PRESSAO ATMOSF?RICA E PROFUNDIDADE H NO ELEMENTO DE TRES N?S
!**********************************************************************

      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GDSF1(3),GDSF2(3),ELV1(3),ELV2(3),EDF1(3),EDF2(3),
     .          ED12(3),ED21(3),ELH(3),EDHT1(3),EDHT2(3)

      AREA=0.5D0*DET
      AREA1=AREA/3.0D0

      DO I=1,3
      EDF1(I)=0.0D0
      EDF2(I)=0.0D0
      ED12(I)=0.0D0
      ED21(I)=0.0D0
      EDHT1(I)=0.0D0
      EDHT2(I)=0.0D0

      DO J=1,3
        EDF1(I)=EDF1(I)+AREA1*GDSF1(J)*ELV1(J)
        EDF2(I)=EDF2(I)+AREA1*GDSF2(J)*ELV2(J)
        ED12(I)=ED12(I)+AREA1*GDSF1(J)*ELV2(J)
        ED21(I)=ED21(I)+AREA1*GDSF2(J)*ELV1(J)
        EDHT1(I)=EDHT1(I)+AREA1*GDSF1(J)*ELH(J)
        EDHT2(I)=EDHT2(I)+AREA1*GDSF2(J)*ELH(J)
      END DO
      END DO

      RETURN
      END


!**********************************************************************
      SUBROUTINE VETORT(GDSF1,GDSF2,DET,ELV1,ELV2,EDF1,EDF2,
     .                  EDPA1,EDPA2,ELH,ED12,ED21,EDH1,EDH2,ELPAT)

!**********************************************************************
!    ESTA ROTINA FAZ A SUAVIZACAO GLOBAL DAS DERIVADAS DA VELOCIDADE,
!    PRESSAO ATMOSF?RICA E PROFUNDIDADE H NO ELEMENTO DE TRES N?S
!**********************************************************************

      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION GDSF1(3),GDSF2(3),ELV1(3),ELV2(3),EDF1(3),EDF2(3),
     .          ED12(3),ED21(3),EDPA1(3),EDPA2(3),EDH1(3),EDH2(3),
     .          ELH(3),ELPAT(3)



      AREA=0.5D0*DET
      AREA1=AREA/3.0D0

      DO I=1,3
      EDF1(I)=0.0D0
      EDF2(I)=0.0D0
      ED12(I)=0.0D0
      ED21(I)=0.0D0
      EDH1(I)=0.0D0
      EDH2(I)=0.0D0
      EDPA1(I)=0.0D0
      EDPA2(I)=0.0D0

      DO J=1,3
        EDF1(I)=EDF1(I)+AREA1*GDSF1(J)*ELV1(J)
        EDF2(I)=EDF2(I)+AREA1*GDSF2(J)*ELV2(J)
        ED12(I)=ED12(I)+AREA1*GDSF1(J)*ELV2(J)
        ED21(I)=ED21(I)+AREA1*GDSF2(J)*ELV1(J)
        EDH1(I)=EDH1(I)+AREA1*GDSF1(J)*ELH(J)
        EDH2(I)=EDH2(I)+AREA1*GDSF2(J)*ELH(J)
        EDPA1(I)=EDPA1(I)+AREA1*GDSF1(J)*ELPAT(J)
        EDPA2(I)=EDPA2(I)+AREA1*GDSF2(J)*ELPAT(J)
      END DO
      END DO

      RETURN
      END



!**********************************************************************
      SUBROUTINE ELEMP(NE,KONE,PP,ELP,PAT,NNOS,NELEM3)
!**********************************************************************
!        CALCULA O VETOR DE PRESSAO DO ELEMENTO PARA O AMORTECIMENTO
!**********************************************************************
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION PP(NNOS),ELP(3),PAT(NNOS),KONE(NELEM3)

      IK=3*(NE-1)

      DO I=1,3
      IND=KONE(IK+I)
      ELP(I)=PP(IND)+PAT(IND)
      END DO

      RETURN
      END

!**********************************************************************
      SUBROUTINE SKELEM(SDM,ELP,SKEN,SKED)
!**********************************************************************
!            CALCULA O VETOR PRESSAO DE AMORTECIMENTO POR ELEMENTO
!**********************************************************************

      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION SDM(3,3),ELP(3),SKEN(3),SKED(3)

      DO L=1,3
      SKEN(L)=0.0D0
      SKED(L)=0.0D0
      DO J=1,3
        SKEN(L)=SKEN(L)+SDM(L,J)*ELP(J)
        SKED(L)=SKED(L)+DABS(SDM(L,J))*ELP(J)
      END DO
      END DO

      RETURN
      END

!**********************************************************************
      SUBROUTINE ELEMS(NE,KONE,SK,SM,NNOS,NELEM3)
!**********************************************************************
!    CALCULA VALOR MEDIO DO SENSOR DE PRESSAO NO ELEMENTO OU CALCULA
!    O VALOR MAXIMO DO SENSOR DE PRESSAO NO ELEMENTO.
!**********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION SK(NNOS), KONE(NELEM3)

!CALCULO DO SENSOR MEDIO

      SM=0.0D0
      IK=3*(NE-1)
      DO I=1,3
         IND=KONE(IK+I)
         SM=SM+SK(IND)
      END DO
      SM=SM/3.D0


      RETURN
      END

!**********************************************************************
      SUBROUTINE DMLCFL(ELV1,ELV2,EHTOT,SDMDT,PLLL1,PLLL2,
     .                  PLLL3,DTE,SM,NE,CC,NELEM)
!**********************************************************************
!                 CALCULA OS VETORES DE AMORTECIMENTO
!**********************************************************************

      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/TEMP/CTIME,DT,DT1
      DIMENSION ELV1(3),ELV2(3),SDMDT(3,3),EHTOT(3),PLLL1(3),PLLL2(3),
     .          PLLL3(3),DTE(NELEM)

      CFL=(DT/DTE(NE))*CC*SM


      DO I=1,3
      PLLL1(I)=0.0D0
      PLLL2(I)=0.0D0
      PLLL3(I)=0.0D0

      DO J=1,3
        PLLL1(I)=PLLL1(I)+SDMDT(I,J)*EHTOT(J)*CFL
        PLLL2(I)=PLLL2(I)+SDMDT(I,J)*ELV1(J)*CFL
        PLLL3(I)=PLLL3(I)+SDMDT(I,J)*ELV2(J)*CFL
      END DO
      END DO

      RETURN
      END

!**********************************************************************
      SUBROUTINE SMOOTH(V1,V2,HTOT,GPDH,GPDV1,GPDV2,GDM, ITYPBC,HMIN,
     .          NNOS)
!**********************************************************************
!                 CALCULA OS VETORES DAS VARI?VEIS SUAVIZADAS
!**********************************************************************

      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/NELPA/NNM,NEM,NBC,NTBN,NSIDC,NEMNPE
      DIMENSION V1(NNOS),V2(NNOS),HTOT(NNOS),GPDV1(NNOS), GPDV2(NNOS),
     .GDM(NNOS),GPDH(NNOS),ITYPBC(NNOS)

      DO I=1,NNM
        IF(ITYPBC(I).EQ.1.OR.ITYPBC(I).EQ.12.OR.ITYPBC(I).EQ.13.
     .      OR.ITYPBC(I).EQ.14.OR.ITYPBC(I).EQ.123.OR.ITYPBC(I).
     .      EQ.124) GPDV1(I) = 0.D0

        IF(ITYPBC(I).EQ.2.OR.ITYPBC(I).EQ.12.OR.ITYPBC(I).EQ.23.
     .     OR.ITYPBC(I).EQ.24.OR.ITYPBC(I).EQ.123.OR.
     .     ITYPBC(I).EQ.124) GPDV2(I) = 0.D0

        IF(ITYPBC(I).EQ.3.OR.ITYPBC(I).EQ.4.OR.ITYPBC(I).EQ.13.
     .      OR.ITYPBC(I).EQ.14.OR.ITYPBC(I).EQ.23.OR.ITYPBC(I).EQ.24.
     .      OR.ITYPBC(I).EQ.123.OR.ITYPBC(I).EQ.124) GPDH(I) = 0.D0

        A1 = V1(I)
        A2 = V2(I)
        A3 = HTOT(I)
        V1(I) = A1 + GPDV1(I) / GDM(I)
        V2(I) = A2 +   GPDV2(I)/ GDM(I)
        HTOT(I) = A3 + GPDH(I)/GDM(I)

        IF(HTOT(I).LE.HMIN) THEN
            V1(I)=0.0D0
            V2(I)=0.0D0
          ENDIF
      END DO

      RETURN
      END

!**********************************************************************
      SUBROUTINE DIVI(SKN,SKD,NNOS)
!**********************************************************************
!                 DIVIDE UM VETOR POR OUTRO(VALOR ABSOLUTO)
!**********************************************************************

      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/NELPA/NNM,NEM,NBC,NTBN,NSIDC,NEMNPE
      DIMENSION SKN(NNOS),SKD(NNOS)

      DO I=1,NNM
      IF(DABS(SKD(I)).LT.0.1E-10)SKN(I)=0.0D0

      SKN(I)=DABS(SKN(I)/SKD(I))

      END DO

      RETURN
      END

!***********************************************************************
      SUBROUTINE CALFT0(NE,KONE,GDSF1,GDSF2,GGDH1,GGDH2,GDHT1,GDHT2,
     .                  GDF1,GDF2,GD12,GD21,H,ELCHE,ELV1,ELV2,ELH,ELF1,
     .                  ELF2,ELFFI,ELPSIX,ELPSIY,FFIEL,PSIXEL,PSIYEL,
     .                  NNOS,NELEM,NELEM3,G)
!***********************************************************************
!             CALCULA AS VARIAVEIS DO PROBLEMA ADJUNTO EM T+1/2
!***********************************************************************
      IMPLICIT REAL*8(A-H,O-Z)
      REAL*8 FIX, FIY, PSIXX, PSIYY

      COMMON/TEMP/CTIME,DT,DT1
      DIMENSION ELF1(3,3),ELF2(3,3),C(3,3),ELV1(3),ELV2(3),ELH(3),
     .          ELPSIX(3),ELPSIY(3),ELFFI(3),GDSF1(3),GDSF2(3),
     .          GDF1(NNOS),GDF2(NNOS),GD12(NNOS), GD21(NNOS),ELCHE(3),
     .          FFIEL(NELEM),PSIXEL(NELEM),PSIYEL(NELEM),KONE(NELEM3),
     .          GGDH1(NNOS),GGDH2(NNOS),GDHT1(NNOS),GDHT2(NNOS),
     .          H(NNOS)


!  CALCULO DO VETOR DE FLUXO E DO TERMO DE FONTE EM 'T'

      F11X=0.D0
      F12X=0.D0
      F13X=0.D0
      F21Y=0.D0
      F22Y=0.D0
      F23Y=0.D0

!    CALCULA LOS VALORES MEDIOS dE LAS VELOCIDADES ALTURAS Y VALORES ADJUNTOS  EN CADA ELEMENTO

      IK=3*(NE-1)

      UM=(ELV1(1)+ELV1(2)+ELV1(3))/3.0D0
      VM=(ELV2(1)+ELV2(2)+ELV2(3))/3.0D0
      HM=(ELH(1)+ELH(2)+ELH(3))/3.0D0
      FFIM=(ELFFI(1)+ELFFI(2)+ELFFI(3))/3.0D0
      PSIXM=(ELPSIX(1)+ELPSIX(2)+ELPSIX(3))/3.0D0
      PSIYM=(ELPSIY(1)+ELPSIY(2)+ELPSIY(3))/3.0D0


      DO I=1,3
      U=ELV1(I)
      V=ELV2(I)
      HT=ELH(I)
      PSIX=ELPSIX(I)
      PSIY=ELPSIY(I)
      FI=ELFFI(I)


!    Los valores derivados se las pasa a las variables directas y adjuntas

      IND=KONE(IK+I)
      FIX=GDHT1(IND) * FI
      FIY=GDHT2(IND) * FI
      PSIXX=GDF1(IND)* PSIX
      PSIYY=GDF1(IND)* PSIY

!CCCC   SE FORMAN F1 y F2 que depende de las derivadas de las adjuntas
!ccccccCONTROLADO

      ELF1(I,1)=(U*U-G*HT)*PSIXX+U*V*PSIYY
      ELF1(I,2)=-FIX-2.0D0*U*PSIXX-V*PSIYY
      ELF1(I,3)=-U*PSIYY
      ELF2(I,1)=U*V*PSIXX+(V*V-G*HT)*PSIYY
      ELF2(I,2)=-V*PSIXX
      ELF2(I,3)=-FIY-U*PSIXX-2.0D0*V*PSIYY


      IND=KONE(IK+I)


      IF (DABS(HT).LT.1.E-10) THEN
        H=HM
      END IF

      IF (DABS(U).LT.1.E-10. AND .DABS(V).LT.1.E-10) THEN
        U=UM
        V=VM
      END IF

      S0X=-GGDH1(IND)*(HT-H(IND))
      S0Y=-GGDH2(IND)*(HT-H(IND))
      HTX=GDHT1(IND)
      HTY=GDHT2(IND)
      UX=GDF1(IND)
      VY=GDF2(IND)
      UY=GD12(IND)
      VX=GD21(IND)
      CHEZY = ELCHE(I)
      SF=DSQRT(U**2+V**2)/(HT*CHEZY**2)
      SFX=SF*U
      SFY=SF*V


!ccccc   AQUI COLOCAMOS LOS TEREMINOS FUENTES

      C(I,1)=(-G*(S0X+2.0D0*SFX)+(2.0*U*UX-G*HTX)*PSIX+(U*VY+V*UY))*PSIX
     .  + (-G*(S0Y+2.0D0*SFY)+(U*VX+V*UX)*PSIY+(2.0D0*V*VY-G*HTY))*PSIY

      C(I,2)=(-G*(((SFX*V)/(U**2+V**2)+SF))+(2.0D0*UX+VY))*PSIX+
     .       (-G*((U/(U**2+V**2))*SFY))+UX* PSIY

      C(I,3)=(-G*((U/(U**2+V**2))*SFX))+    UY*PSIX +
     .       (-G*(((SFY*V)/(U**2+V**2)+SF))+(2.0D0*VY+UX))*PSIY
      END DO

      CM1=(C(1,1)+C(2,1)+C(3,1))/3.0D0
      CM2=(C(1,2)+C(2,2)+C(3,2))/3.0D0
      CM3=(C(1,3)+C(2,3)+C(3,3))/3.0D0

      DO I=1,3
      F11X=F11X+GDSF1(I)*ELF1(I,1)
      F12X=F12X+GDSF1(I)*ELF1(I,2)
      F13X=F13X+GDSF1(I)*ELF1(I,3)
      F21Y=F21Y+GDSF2(I)*ELF2(I,1)
      F22Y=F22Y+GDSF2(I)*ELF2(I,2)
      F23Y=F23Y+GDSF2(I)*ELF2(I,3)
      END DO

      FFIEL(NE)=FFIM-DT1*(F11X+F21Y+CM1)
      PSIXEL(NE)=PSIXM-DT1*(F12X+F22Y+CM2)
      PSIYEL(NE)=PSIYM-DT1*(F13X+F23Y+CM3)

      RETURN
      END

!**************************************************************************
      SUBROUTINE FLVT0(NE,KONE,FFIEL,PSIXEL,PSIYEL,GDSF1,GDSF2,H,
     .               ELCHE,ELV1,ELV2,ELH,GGDH1,GGDH2,F11,F12,F13,
     .   F21,F22,F23,NODO,HMAX,PPL1,PPL2,PPL3,NNOS,NELEM,
     .                 NELEM3,G)
!**************************************************************************
!      CALCULA AS VARIVEIS DO PROBLEMA ADJUNTO EM T=T+DT
!**************************************************************************
      IMPLICIT REAL*8(A-H,O-Z)
      REAL*8 FIX, FIY, PSIXX, PSIYY

      COMMON/TEMP/CTIME,DT,DT1
      DIMENSION FFIEL(NELEM),PSIXEL(NELEM),PSIYEL(NELEM),GDSF1(3),
     .        GDSF2(3),S0X(3),S0Y(3),PPL1(3),PPL2(3),PPL3(3),
     .        ELV1(3),ELV2(3),ELH(3), ELCHE(3),KONE(NELEM3),
     .          GGDH1(NNOS),GGDH2(NNOS),H(NNOS), C(3,3)


      IK=3*(NE-1)
      CHEZY=(ELCHE(1)+ELCHE(2)+ELCHE(3))/3.0D0
      U=(ELV1(1)+ELV1(2)+ELV1(3))/3.0D0
      V=(ELV2(1)+ELV2(2)+ELV2(3))/3.0D0
      HT=(ELH(1)+ELH(2)+ELH(3))/3.0D0
      FI=FFIEL(NE)
      PSIX=PSIXEL(NE)
      PSIY=PSIYEL(NE)
      SFM=DSQRT(U**2+V**2)/(HT*CHEZY**2)
!    SF=DSQRT(U**2+V**2)/(HT*CHEZY**2)

      SFX=SFM*U
      SFY=SFM*V
      HTX=0.D0
      HTY=0.D0
      UX=0.D0
      UY=0.D0
      VX=0.D0
      VY=0.D0

      DO I=1,3

      IND=KONE(IK+I)

      FIX=GGDH1(IND) * FI
      FIY=GGDH2(IND) * FI
      PSIXX=GDSF1(IND)* PSIX
      PSIXY=GDSF2(IND)* PSIY
      PSIYX=GDSF1(IND)* PSIX
      PSIYY=GDSF1(IND)* PSIY

      S0X(I)=-GGDH1(IND)*(ELH(I)-H(IND))
      S0Y(I)=-GGDH2(IND)*(ELH(I)-H(IND))
      HTX=HTX+GDSF1(I)*ELH(I)
      HTY=HTY+GDSF2(I)*ELH(I)
      UX=UX+GDSF1(I)*ELV1(I)
      UY=UY+GDSF2(I)*ELV1(I)
      VX=VX+GDSF1(I)*ELV2(I)
      VY=VY+GDSF2(I)*ELV2(I)

      C(I,1)=(-G*(S0X(I)+2.0D0*SFX)+(2.0*U*UX-G*HTX)*PSIX+(U*VY+V*UY))
     .*PSIX
     .+ (-G*(S0Y(I)+2.0D0*SFY)+(U*VX+V*UX)*PSIY+(2.0D0*V*VY-G*HTY))*PSIY

      C(I,2)=(-G*(((SFX*V)/(U**2+V**2)+SFM))+(2.0D0*UX+VY))*PSIX+
     .   (-G*((U/(U**2+V**2))*SFY))+UX* PSIY

      C(I,3)=(-G*((U/(U**2+V**2))*SFX))+  UY*PSIX +
     .  (-G*(((SFY*V)/(U**2+V**2)+SFM))+(2.0D0*VY+UX))*PSIY
      END DO

      F11=(U*U-G*HT)*PSIXX+U*V*PSIYY
      F12=-FIX-2.0D0*U*PSIXX-V*PSIYY
      F13=-U*PSIYY
      F21=U*V*PSIXX +(V*V-G*HT)*PSIYY
      F22=-V*PSIXX
      F23=-FIY-U*PSIXX-2.0D0*V*PSIYY

      S0XM=(S0X(1)+S0X(2)+S0X(3))/3.0D0
      S0YM=(S0Y(1)+S0Y(2)+S0Y(3))/3.0D0

      DO I=1,3
      IND = KONE(IK+I)
      IF(IND.EQ.NODO.AND.HT.GE.HMAX)THEN
      HT = ELH(I)
      DH =( HT-HMAX )

      ELSE
      DH = 0.0D0
      END IF

      PPL1(I)=(GDSF1(I)*F11+GDSF2(I)*F21-C(I,1)/3.0D0-DH)*DT
      PPL2(I)=(GDSF1(I)*F12+GDSF2(I)*F22-C(I,2)/3.0D0)*DT
      PPL3(I)=(GDSF1(I)*F13+GDSF2(I)*F23-C(I,3)/3.0D0)*DT

      END DO

      RETURN
      END
