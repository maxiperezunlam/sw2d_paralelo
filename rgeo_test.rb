require 'rgeo'

f = RGeo::Cartesian::Factory.new
p1 = [[0,0],[5,0],[5,5]]
p2 = [[3,1],[3,6],[7,6]]

polygons = []
[p1,p2].each do |ps|
	points = []
	ps.each do |x,y|
		points << f.point(x,y)
	end
	polygons << f.polygon(f.linear_ring(points))
end
