#!/bin/bash
mkdir -p debugging
num1=$(ls debugging/ | sort -n | tail -n 1 | tr --delete [:alpha:])
num1=$(expr $num1 + 1)
mkdir -p "debugging/dbg$num1"
mv --force ASMDBG* debugging/dbg$num1 2> /dev/null
mv --force PARDBG* debugging/dbg$num1 2> /dev/null