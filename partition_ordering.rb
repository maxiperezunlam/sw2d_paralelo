#!/bin/ruby
require "awesome_print"
DEBUG = false

def adjacents?(i1,j1,i2,j2, part_per_proc, npart)
	maxindex = npart - 1
	e1 = i1 * part_per_proc + j1
	e2 = i2 * part_per_proc + j2
	
	e1 == e2 || e1 + 1 == e2 || e1 - 1 == e2 || (e1 == 0 && maxindex == e2 || e2 == 0 && maxindex == e1)
end

#Leemos el encabezado del archivo de particiones para poder definir la estructura donde guardar
#los datos y realizar el analisis
#Variables necesarias
nproc = 0 #cantidad de procesadores
npart = 0 #cantidad de particiones
partitions = [] #listado con todas las particiones

nreadedlines = 0
partition_filepath = "partitions2.txt"

File.open(partition_filepath, "r").each_line do |line|
	nreadedlines += 1
	#lei la cantidad de procesadores y de particiones
	if nreadedlines == 1
		nproc, npart = line.split(" ").map {|e| e.to_i }
	elsif nreadedlines == 3 #lei todas las particiones
		partitions = line.split(" ").map {|e| e.to_i }
	else
		break if nreadedlines > 3
	end
end

ap nproc if DEBUG
ap npart if DEBUG
ap partitions if DEBUG

part_matrix = []
orderings = []
part_per_proc = npart / nproc

#Mapeamos la informacion en una matriz donde las filas representa al id de thread y las columnas al 
#id de particion. Cada fila-columna contendra la cantidad de elementos de la particion asignada previamente al procesador. Un ejemplo es el siguiente:
#  		PART1	PART2	PART3	PART4	
#  THREAD 1	100	80	400	200
#  THREAD 2	80	100	200	400
#  THREAD 3	100	111	400	222
#  THREAD 4	110	342	202	101
#Desde esta matriz podemos tambien analizar facilmente la adyacencia entre particiones.
nproc.times do |i|
	part_matrix[i] = []
	orderings[i] = []
	part_per_proc.times do |j|
		part_matrix[i][j] = partitions[part_per_proc * i  + j]
	end
end

ap part_matrix if DEBUG
part_matrix_aux = part_matrix.clone

#La idea es una funcion Nat Cuadrado -> Nat Cuadrado, es decir que partimos de una matriz de particiones con su tamaños y llegamos a una matriz que nos entrega el ordenamiento "optimo" a ser realizadoque cumple los siguientes requisitos:
# 1. No procesa al mismo tiempo particiones adyacentes
# 2. La cantidad de elementos a ser procesados en un instante de tiempo es lo mas balanceado posible
#    a menos que se viole la restriccion 1. En ese caso se buscara la siguiente particion.
# La idea es que globalmente tendera a ser el ordenamiento optimo de ejecución al balancear las cargas. En caso de que sea totalmente homogeneo deberia ser el ordenamiento optimo.
#Cabe observar que esta funcion es parcial y no total ya que dado el caso que se encuentre una adyacencia en el ordenamiento y sea el ultimo ciclo a procesar el ordenamiento sera invalido, es decir
#dada una maquina de turing esta podria no parar nunca. Problema parcialmente decidible.
#Si se quiere evitar hay que tomar algun criterio para aplicar que elimine el caso antes de llegar al ultimo ciclo de analisis.
index_max_ant = index_max = index_max_2 = 0
size_max = size_max_2 = 0

part_per_proc.times do |i|
	nproc.times do |j|
		#Como siempre entre procesadores excepto entre el primero y el ultimo la adyacencia se da entre el ultimo elemento del proc i con el primer elemento del procesador i+1. Lo que hago es eliminar en el primer ciclo de analisis el caso borde. Para ello utilizo una variante del algoritmo diferente a la propuesta para los siguientes pasos que consiste en:
# Para el primer procesador fijo la primera particion
# para el segundo y los n-1 siguientes busco los mas cercanos en cantidad de elementos. Entre estos y el primer procesador no puede existir adyacencia. Podria existir entre el 2 y el 3 por ejemplo pero el analisis es siempre secuencial y hay un unico par de elementos que son adyacentes por lo que puedo eliminar el conflicto buscando el siguiente mas proximo en cantidad de elementos.
# para el ultimo procesador elijo el mas cercano excluyendo el ultimo elemento. De esta manera me aseguro que no pueden ser a partir de este punto nunca mas adyacentes la particion asignada al primer procesador y la del ultimo.
		if i.zero?
			if j.zero?
				size_max, index_max = part_matrix_aux[j].each_with_index.first
				ap "es nulo #{i},#{j}: #{index_max.nil?}"
				part_matrix_aux[j][index_max] = 0
     				orderings[j][i] = index_max
				index_max_ant = 0
				ap index_max_ant
			else
				ap "Distancias #{index_max_ant}"
				if j < nproc
					k = part_per_proc	
				else
					k = part_per_proc - 1
				end
				
				distances = part_matrix_aux[j][0..k].map {|e| (e - part_matrix[0][0]).abs }
				index_max = distances.each_with_index.min.last
				ap index_max
				size_max = part_matrix[j][index_max]	
				ap size_max
				distances[index_max] = 2**(0.size * 8 - 2) - 1 #MAX INTEGER VALUE
				
				ap "#{j},#{index_max},#{j-1},#{index_max_ant},#{part_per_proc},#{nproc}"
				if adjacents?(j, index_max, j-1, index_max_ant, part_per_proc, npart)
					index_max = distances.each_with_index.min.last
				
				end	
				
				part_matrix_aux[j][index_max] = 0
     				orderings[j][i] = index_max
				index_max_ant = index_max
			end	
		else
			#Buscamos el maximo del primer procesador 
			#necesitamos el indice de su maxima particion y el tamaño de la misma
			size_max, index_max = part_matrix_aux[j].each_with_index.max
			ap "es nulo #{i},#{j}: #{index_max.nil?}"
			#Marcamos al elemento para que no vuelva a aparecer en la busqueda
			part_matrix_aux[j][index_max] = 0
		
			if j.zero? #el primer procesador siempre lo asigno directamente sin analizar
     				orderings[j][i] = index_max
				index_max_ant = index_max
			else #Al resto de los procesadores hago el analisis correspondiente
				ap "CASO ESPECIAL a." if j == 1 && i == 2
				#En el caso aqui analizado siempre son adyacentes la ultima particion del procesador i con la primera particion del procesador j
				if adjacents?(j,index_max,j-1,index_max_ant,part_per_proc, npart)
				
					ap "CASO ESPECIAL a.a" if j == 1 && i == 2
					size_max_2, index_max_2 = part_matrix_aux[j].each_with_index.max
			        	#restaura el elemento primeramente eliminado para que vuelva a aparecer
					part_matrix_aux[j][index_max] = size_max
					#Marcamos al nuevo "maximo"
					part_matrix_aux[j][index_max_2] = 0
					#Asigno al nuevo maximo como maximo anterior
					index_max_ant = index_max_2		
					orderings[j][i] = index_max_2
				else
				
						
					ap "CASO ESPECIAL a.c" if j == 1 && i == 2
     					orderings[j][i] = index_max
					index_max_ant = index_max
				end	
			end
		end
	end
			
end


ap orderings

#Tests 
ap adjacents?(0,0,3,5, part_per_proc, npart) if DEBUG
ap adjacents?(0,0,0,1, part_per_proc, npart) if DEBUG
ap adjacents?(1,0,0,5, part_per_proc, npart) if DEBUG
ap adjacents?(1,0,1,1, part_per_proc, npart) if DEBUG
ap adjacents?(1,1,1,0, part_per_proc, npart) if DEBUG
ap adjacents?(1,1,1,2, part_per_proc, npart) if DEBUG

File.open("ordering.txt","w") do |f|
	nproc.times do |i|
		f.write "#{orderings[i].map {|e| e + i * part_per_proc}.join(" ")}\n"
	end
end
