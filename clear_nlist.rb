rawfile = File.readlines ARGV[0]
filtered = rawfile.map(&:chomp).select {|e| e =~ /\d/}
filtered[1..filtered.size].each do |l| 
  result =  l.chomp.split(" ").last(3).first(2)
  str = "%16.6f%16.6f" % result
  puts str
end