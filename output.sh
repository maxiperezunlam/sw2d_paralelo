#!/bin/bash
mkdir -p outputs
num1=$(ls outputs/ | sort -n | tail -n 1 | tr --delete [:alpha:])
num1=$(expr $num1 + 1)
mkdir -p "outputs/output$num1"
mv --force chuS* outputs/output$num1 2> /dev/null
mv --force chu*.plt outputs/output$num1 2> /dev/null