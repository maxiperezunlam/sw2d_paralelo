require 'awesome_print'
require 'mongo'

client = Mongo::Client.new([ '127.0.0.1:27017'  ], :database => 'c181_islaDensificada')
db = client.database
collection = client[:elements]

def get_right_border_element(collection)
        collection.find(
                {
                        :$or => [
                                {
                                  :$and => [
                                        {
                                                :"nodes.node1.x" => { :$regex => /^200\.00.*$/}
                                        },
                                        {
                                                :"nodes.node2.x" => { :$regex => /^200\.00.*$/}
                                        }
                                  ]
                                },
                                {
                                  :$and => [
                                        {
                                                :"nodes.node1.x" => { :$regex => /^200\.00.*$/}
                                        },
                                        {
                                                :"nodes.node3.x" => { :$regex => /^200\.00.*$/}
                                        }
                                  ]
                                },
                                {
                                  :$and => [
                                        {
                                                :"nodes.node2.x" => { :$regex => /^200\.00.*$/}
                                        },
                                        {
                                                :"nodes.node3.x" => { :$regex => /^200\.00.*$/}
                                        }
                                  ]
                                }
                        ]
                },
                {
                        :element_number => 1
                }
        )

end

def get_left_border_element(collection)
        collection.find(
                {
                        :$or => [
                                {
                                  :$and => [
                                        {
                                                :"nodes.node1.x" => { :$regex => /^0\.00.*$/}
                                        },
                                        {
                                                :"nodes.node2.x" => { :$regex => /^0\.00.*$/}
                                        }
                                  ]
                                },
                                {
                                  :$and => [
                                        {
                                                :"nodes.node1.x" => { :$regex => /^0\.00.*$/}
                                        },
                                        {
                                                :"nodes.node3.x" => { :$regex => /^0\.00.*$/}
                                        }
                                  ]
                                },
                                {
                                  :$and => [
                                        {
                                                :"nodes.node2.x" => { :$regex => /^0\.00.*$/}
                                        },
                                        {
                                                :"nodes.node3.x" => { :$regex => /^0\.00.*$/}
                                        }
                                  ]
                                }
                        ]
                },
                {
                        :element_number => 1
                }
        )
end

def get_top_border_element(collection)
        collection.find(
                {
                        :$or => [
                                {
                                  :$and => [
                                        {
                                                :"nodes.node1.y" => { :$regex => /^40\.00.*$/}
                                        },
                                        {
                                                :"nodes.node2.y" => { :$regex => /^40\.00.*$/}
                                        }
                                  ]
                                },
                                {
                                  :$and => [
                                        {
                                                :"nodes.node1.y" => { :$regex => /^40\.00.*$/}
                                        },
                                        {
                                                :"nodes.node3.y" => { :$regex => /^40\.00.*$/}
                                        }
                                  ]
                                },
                                {
                                  :$and => [
                                        {
                                                :"nodes.node2.y" => { :$regex => /^40\.00.*$/}
                                        },
                                        {
                                                :"nodes.node3.y" => { :$regex => /^40\.00.*$/}
                                        }
                                  ]
                                }
                        ]
                },
                {
                        :element_number => 1
                }
        )
end

def get_bottom_border_element(collection)
        collection.find(
                {
                        :$or => [
                                {
                                  :$and => [
                                        {
                                                :"nodes.node1.y" => { :$regex => /^0\.00.*$/}
                                        },
                                        {
                                                :"nodes.node2.y" => { :$regex => /^0\.00.*$/}
                                        }
                                  ]
                                },
                                {
                                  :$and => [
                                        {
                                                :"nodes.node1.y" => { :$regex => /^0\.00.*$/}
                                        },
                                        {
                                                :"nodes.node3.y" => { :$regex => /^0\.00.*$/}
                                        }
                                  ]
                                },
                                {
                                  :and => [
                                        {
                                                :"nodes.node2.y" =>  { :$regex => /^0\.00.*$/}
                                        },
                                        {
                                                :"nodes.node3.y" => { :$regex => /^0\.00.*$/}
                                        }
                                  ]
                                }
                        ]
                },
                {
                        :element_number => 1
                }
        )
end

rbe = get_right_border_element(collection).map {|e| e["element_number"]}
lbe = get_left_border_element(collection).map {|e| e["element_number"]}
tbe = get_top_border_element(collection).map {|e| e["element_number"]}
bbe = get_bottom_border_element(collection).map {|e| e["element_number"]}

ap "Cantidad de elementos derecha #{rbe.size}"
rbe.each {|e| puts e }
ap "Cantidad de elementos izquierda #{lbe.size}"
lbe.each {|e| puts e }
ap "Cantidad de elementos superiores #{tbe.size}"
tbe.each {|e| puts e }
ap "Cantidad de elemento inferiores #{bbe.size}"
bbe.each {|e| puts e }
#ap "elementos inferiores #{bbe.to_a}"
