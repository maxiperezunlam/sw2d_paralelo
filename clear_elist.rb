rawfile = File.readlines ARGV[0]
filtered = rawfile.select {|e| e != "\r\n" && e =~ /\d/}
filtered.each do |l| 
  result =  l.chomp.split(" ").last(4).first(3)
  str = "%12d%12d%12d" % result
  puts str
end