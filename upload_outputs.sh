#!/bin/bash

if [[ -z "$3" ]]
then
	dst="Investigacion/Salidas"
else
	dst="$3"
fi

if [[ -z "$2" ]]
then
	src="/home/maximiliano/Experimentos/ResultadosTinetti/v09-rodapar/v10-parloop/outputs"
else
	src="$2"
fi

if [[ "$1" -eq "--files" ]]
then
	for filename in $(ls $2)
	do
		put "$filename" "$dst"
	done
else
	if [[ "$1" -eq "--file" ]]
	then
		put "$src" "$dst"
	else
		put "$src" "$dst"
	fi
fi
