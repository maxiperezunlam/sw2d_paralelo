require 'awesome_print'
require 'mongo'

DEBUG = false
factor = 2
#Conexion a la base de datos de coordenadas y topologia de la malla
Mongo::Logger.logger.level = Logger::FATAL
client = Mongo::Client.new([ '127.0.0.1:27017'  ], :database => 'c181_islaDensificada')
db = client.database
collection = client[:elements]

#Funcion de comparacion con cierta precision
if false
class Float
	EPSILON = 1e-307

	def ==(x)
		(self-x).abs < EPSILON
	end
	
	def equals?(x,tolerance=EPSILON)
		(self-x).abs < tolerance
	end

	def nearly_equal?(b, tolerance=EPSILON)
		absA = self.abs
		absB = b.abs
		diff = (self-b).abs

		if self == b 
			true
		elsif self == 0 || b == 0 || diff < Float::MIN
			diff < (tolerance * Float::MIN)
		else
			diff / [(absA + absB), Float::MAX].min < tolerance
		end
	end
end
end

#Funciones
def get_partition(collection, min,max, xmax, ymax, center)
        centerx = center["x"]
        centery = center["y"]

        collection.find(
                {
                        :$or => [
                                {
                                  :$and => [
                                        {
                                                :"nodes.node1.x" => { :$regex => /^200\.00.*$/}
                                        },
                                        {
                                                :"nodes.node2.x" => { :$regex => /^200\.00.*$/}
                                        }
                                  ]
                                },
                                {
                                  :$and => [
                                        {
                                                :"nodes.node1.x" => { :$regex => /^200\.00.*$/}
                                        },
                                        {
                                                :"nodes.node3.x" => { :$regex => /^200\.00.*$/}
                                        }
                                  ]
                                },
                                {
                                  :$and => [
                                        {
                                                :"nodes.node2.x" => { :$regex => /^200\.00.*$/}
                                        },
                                        {
                                                :"nodes.node3.x" => { :$regex => /^200\.00.*$/}
                                        }
                                  ]
                                }
                        ]
                },
                {
                        :element_number => 1
                }
        )

end

#Calcula la coordenadas del tercer vertice del triangulo
def find_vertex(radio,center, theta)
  x = radio * Math.cos(theta) + center["x"]
  y = radio * Math.sin(theta) + center["y"]

  {"x" => x,"y" => y}
end

#Elimina duplicados entre particiones. La particion que elije para eliminar
#en caso de encontrar duplicados es la que tiene mayor cantidad de elementos
def remove_duplicates(partitions)
  partitions
end

def get_search_space(collection, center,theta,xmax,ymax)
  collection.find({})
end

def get_element_from_db(collection, element_number)
  collection.find({:"element_number" => {"$eq" => element_number}})
end

#Funcion auxiliar para conocer si un punto pertenece a un triangulo
def sign(p1,p2,p3)
  a1 = (p1["x"] - p3["x"])
  ap a1 if DEBUG
  a2 = (p2["y"] - p3["y"])
  ap a2 if DEBUG
  a = a1 * a2
  ap a if DEBUG

  b1 = (p2["x"] - p3["x"])
  ap b1 if DEBUG
  b2 = (p1["y"] - p3["y"])
  ap b2 if DEBUG
  b = b1 * b2
  ap b if DEBUG
  a - b
end

def included_in_triangle?(node, a,b,c)
  ap "node: #{node}" if DEBUG
  ap "a: #{a}" if DEBUG
  ap "b: #{b}" if DEBUG
  ap "c: #{c}" if DEBUG


  s1 = sign(node, a,b)
  s2 = sign(node, b,c)
  s3 = sign(node, c,a)

  b1 = s1 < 0.0 #|| s1.zero?
  b2 = s2 < 0.0 #|| s2.zero?
  b3 = s3 < 0.0 #|| s3.zero?

  b1 == b2 && b2 == b3 
end

def verify_partitions(part)
  v = true

  part.each do |p|
     sub = part - [p]
     sub.each do |p2|
      v = p & p2 == []
      return v if !v
     end
     return v if !v
  end

  return v
end


def extract_partition_from_mesh(collection, center, theta, xmax, ymax, vert1,vert2,vert3, npart=0, debug=false)
  #Obtenemos el espacio de busqueda reducido
  search_space = get_search_space(collection, center, theta, xmax, ymax)

  #Analizamos cada elemento del espacio de busqueda. Si al menos dos de sus
  #nodos pertenecen al triangulo definido con vertices (vert1,vert2,vert3)
  #Lo incluimos en la particion. De lo contrario lo descartamos
  current_partition = []

  search_space.each do |element|
    #Obtenemos los nodos del elemento
      node1 = element["nodes"]["node1"];
      node2 = element["nodes"]["node2"];
      node3 = element["nodes"]["node3"];

      node1 = {"x" => node1["x"].to_f, "y" => node1["y"].to_f}
      node2 = {"x" => node2["x"].to_f, "y" => node2["y"].to_f}
      node3 = {"x" => node3["x"].to_f, "y" => node3["y"].to_f}
      ap node1 if DEBUG
      ap node2 if DEBUG
      ap node3 if DEBUG

      if node1.nil? or node2.nil? or node3.nil?
        puts "El elemento #{element} tiene nulos"
        next
      end
      #Analizamos el primer nodo 
      q1 = included_in_triangle?(node1, vert1,vert2, vert3)
      #Analizamos el segundo nodo 
      q2 = included_in_triangle?(node2, vert1,vert2, vert3)
      #Analizamos el tercer nodo 
      q3 = included_in_triangle?(node3, vert1,vert2, vert3)

      is_included = (q1 && q2) || (q1 && q3) || (q2 && q3)

      element_number = element["element_number"].to_i

      if(is_included)
        current_partition << element_number
      else  
	if debug || DEBUG
		if npart == 0 && element_number == 50
			ap "node1: #{node1}"
			ap "node2: #{node2}"
			ap "node3: #{node3}"
			ap "q1: #{q1}"
			ap "q2: #{q2}"
			ap "q3: #{q3}"	
		end
	end
      end
   end;

   current_partition
end

def element_nodes_included_in_triangle(element, vert1,vert2,vert3)
      #Obtenemos los nodos del elemento
      node1 = element["nodes"]["node1"];
      node2 = element["nodes"]["node2"];
      node3 = element["nodes"]["node3"];

      node1 = {"x" => node1["x"].to_f, "y" => node1["y"].to_f}
      node2 = {"x" => node2["x"].to_f, "y" => node2["y"].to_f}
      node3 = {"x" => node3["x"].to_f, "y" => node3["y"].to_f}

      if node1.nil? or node2.nil? or node3.nil?
        puts "El elemento #{element} tiene nulos"
      end

      #Analizamos el primer nodo 
      q1 = included_in_triangle?(node1, vert1,vert2, vert3)
      #Analizamos el segundo nodo 
      q2 = included_in_triangle?(node2, vert1,vert2, vert3)
      #Analizamos el tercer nodo 
      q3 = included_in_triangle?(node3, vert1,vert2, vert3)

      return [q1,q2,q3]
end

def validate_partitions(partitions, nelems)
	#Agregar chequeo que tienen que ser 4400 elementos todas las particiones
	#en realidad los 4400 elements son nelems que es el numero total de elementos de la malla
  	qpElements =  partitions.reduce(0) {|acc, e| acc+=e.size }

  	#Si no tiene la misma cantidad de elementos elimino duplicados
  	#tengo que verlo porque podria dar la suma de elementos y no estar todos los elementos?
  	#en principio deberia de haber elementos de mas
  	if qpElements != nelems
  	 partitions = remove_duplicates(partitions)
  	end
  	
	#ordenamos para que las particiones con cantidad de elementos similares
        #queden juntas
 	#lpartitions = partitions.sort_by {|e| e.size }.reverse

  	#Realizamos una verificacion para ver si todo va bien. 
  	intersection_is_empty = verify_partitions(partitions)
  
	return [qpElements, partitions, intersection_is_empty]
end

def execute(collection,np = nil, fpart = nil, delta = 0.25, _nmaxsteps: 1)
	#Variables del problema
	nprocs = np || 8
	factorproc = fpart || 1
	npartitions = factorproc * nprocs #esto podria cambiar si quiero hacer el interleave
	pi = Math::PI
	
	#En realidad el offset es el angulo theta de division
	offset = (2.0 * pi)/npartitions
	xmax = 200.0
	ymax = 40.0
	partitions = []
	nelems = 4400
	center = {"x" => xmax / 2.0, "y" => ymax / 2.0}
	
	coeffr = 1.0 #indica una proporcion del tamaño de la malla como radio
	radio = xmax * coeffr

	#En realidad es el vertice 2 que en principio se propone pero para evitar
	#una asignacion de mas lo definimos como el tercer vertice asignandolo como
	#se hara para las particiones consecutivas
	vert3 = find_vertex(radio, center, 0.0)
	geoPart = {}

  	1.upto(npartitions) do |i|
  		theta = i * offset
		theta0 = theta - (i-1) * offset
  		vert1 = center
  		vert2 = vert3
  		vert3 = find_vertex(radio, center, theta)
  		#ap "Vertices #{vert1} #{vert2} #{vert3}"
		ap "Paso 1 Theta #{theta}"
		ap "Paso 1 Theta0 #{theta0}"

  		geoPart.merge!({(i-1) => { "vertex" => [vert1,vert2, vert3], "offset" => offset, "theta" => theta, "theta0" => theta0}})

  		#Buscamos la particion generada a partir de los datos
  		partitions << extract_partition_from_mesh(collection, center, theta, xmax, ymax, vert1,vert2,vert3)
  	end

	qpElements, partitions, intersection_is_empty = validate_partitions(partitions, nelems)
	puts "PASO 1"
	puts "Verificacion de elementos: #{qpElements == nelems} #{qpElements}"
	puts "Verificacion interseccion: #{intersection_is_empty}"

  	#Luego de verificar que la primer parte del algoritmo es correcta
  	#Realizamos un refinamiento para balancear la carga.
  	#La idea es buscar la particion con menor cantidad de elementos
  	#Cada particion es en si mismo un espacio de particionamiento por lo que 
  	#ningun elemento de la particion q1 puede pasar a la particion q2.
  	#El algoritmo se basa en el refinamiento en N pasos. Se debe garantizar que 
  	#la cantidad de particiones total es multiplo de la cantidad de procesadores.
  	#con el objetivo de aprovechar la maxima cantidad de recursos disponibles para
  	#la resolucion del problema.
  	
  
  	#Por parametros recibimos un valor de delta. Este delta es el que analiza si una particion hay que  #reparticionarla o no. Procedemos a reparticionar/refinar lo realizado
	#Por el momento funciona con un solo paso
  	nmaxsteps = _nmaxsteps || 1
  	partitions, seq_elems = refine_partitions(collection, partitions, [], geoPart, radio, xmax, ymax, nprocs, delta, 0, nmaxsteps)
  	npartitions = partitions.size

	qpElements, partitions, intersection_is_empty = validate_partitions(partitions, nelems)
	puts "PASO 2"
	puts "Verificacion de elementos: #{qpElements == nelems} #{qpElements}"
	puts "Verificacion interseccion: #{intersection_is_empty}"
	
  	#imprimimos un archivo con los elementos de cada particion
  	filename = "partitions.txt"
	cl = 0
  	File.open(filename, "w") do |f|
    		f.write("#{nprocs} #{npartitions}\n")
		cl+=1
		if seq_elems.empty?
			f.write("0 0\n")
		else
			f.write("1 #{seq_elems.size}\n")
		end
		cl += 1
		sizes = partitions.map {|e| e.size }
		sizes.each do |s|
			f.write("#{s} ")
		end
		f.write("#{seq_elems.size}")
	        f.write("\n")	
		cl += 1

	    	lines = partitions.map {|e| "#{e.sort.join(' ')}"}
    		lines.each do |l|
      			f.write("#{l}\n")
			cl += 1
    		end
		
		if !seq_elems.empty?
			seq_elems_str = seq_elems.sort.join(' ')
			f.write("#{seq_elems_str}")
			cl += 1
			puts seq_elems_str
		end
  	end 

  	#Imprimimos un resumen de los realizado
  	ap "Numero de procesadores #{nprocs}"
  	ap "Numero de particiones #{npartitions}"
  	i = 1
  	partitions.each do |p|
  	  ap "Particion ##{i}"
  	  ap "Cantidad de elementos: #{p.size}"
  	  i += 1
  	end
  	ap "Suma de elementos de las particiones: #{qpElements}"
  	ap "La interseccion es vacia #{intersection_is_empty}"
  	ap "================================="
  	true
end

#Algoritmo de refinamiento por cortes geometricos bajo la suposicion que en cada particion
#puedo dividirlo axialmente hasta encontrar una configuracion balanceada teniendo en cuenta
#el delta 
def refine_partitions(collection, partitions,seq_elems, geoPart,radio,xmax,ymax, nproc, delta, nstep, nmaxsteps = 1)
  if nstep == nmaxsteps

     qpElements, partitions, intersection_is_empty = validate_partitions(partitions, 4400)
     puts "PASO interno #{nstep}"
     puts "Verificacion de elementos: #{qpElements == 4400}"
     puts "Verificacion interseccion: #{intersection_is_empty}"

     return [partitions,seq_elems]
  else 
  	#Buscamos la minima particion
	puts "xmax: #{xmax}"
	puts "ymax: #{ymax}"
	minidx = 0
	minsize = partitions.first.size
	partitions.each_with_index do |p,i|
		ap "Nro part #{i}, nelems: #{p.size}"
		if p.size < minsize
			minidx = i
			minsize = p.size
		end
	end
	averagesize = partitions.map {|e| e.size}.sort.first(nproc).sum / nproc
        puts "Promedio sizes #{averagesize}"
	
  	minpart = [minidx, partitions[minidx]] #partitions.each_with_index.min
  	auxGeoPart = {}
	ap "minpart #{[minidx, minsize]}"

	#Extraemos el indice de la minima particion dentro del array de particiones
  	#y la cantidad de elementos de esta
 	minpartidx = minpart.first
 	minpartsize = averagesize #minpart.last.size
	delta_elems = minpartsize * delta
	auxpartitions = []
	auxGeoPartIdx = 0
	ap "Minimo idx #{minpartidx}"
	ap "Minimo size #{minpartsize}"

	#ap "geoParts: #{geoPart}"

	plusdelta = minpartsize + delta_elems
	minusdelta = minpartsize - delta_elems
	sumdiffs = 0
	tofixedelements = {}

	partitions.each_with_index do |part,i|
		current_size = part.size
		#Extraigo los vertices iniciales
		vertex = geoPart[i]["vertex"]
		#Calculamos el nuevo angulo maximo de particionamiento
		#El primer y segundo punto de la primer subparticion son los puntos
		#v1 y v2 de la particion original.
		offset0 = geoPart[i]["offset"]
		thetaOriginal = geoPart[i]["theta"]
		theta0 = geoPart[i]["theta0"]
			

		unless (current_size >= minusdelta && current_size <= plusdelta) || current_size <= minpartsize
			#A reparticionar
			puts "*************** Nro particion #{i} ***************"
			puts "Cantidad Elementos #{part.size}"

			puts "Theta original #{thetaOriginal}"
			puts "theta0 #{theta0}"
			#Calculo la nueva cantidad de particiones para esta subparticion
			npart = (current_size / minpartsize.to_f).ceil
			#Calculo el angulo por el cual dividir
			offset = theta0 / npart.to_f
			puts "npart: #{npart}"
			puts "offset: #{offset}"
			inicio = thetaOriginal - theta0
			puts "Angulo inicial #{inicio}"
			#En este caso el centro es siempre v1, inicializo v3 como v2 como hicimos
			#en el algoritmo original para poder girar axialmente
			v1 = vertex[0]
			v3 = find_vertex(radio, v1, inicio)

			diffparts = []
			#Recalculo las particiones para esa subparticion	
			1.upto(npart) do |j|
				#Siempre el inicial original (NO CAMBIAR)
				theta = [inicio + j * offset, thetaOriginal].min
				#El movimiento de theta0 lo hacemos al cambiar v2 con v3 eso ya me 
				puts "Theta refinamiento: #{theta}"
				#delimita la zona de busqueda
				v2 = v3
				#Calculamos el tercer vertice con el numero theta
				v3 = find_vertex(radio + 5.0, v1, theta)
				puts "Vertice 1: #{v1}"
				puts "Vertice 2: #{v2}"
				puts "Vertice 3: #{v3}"

				#Calculamos el indice para el hash de geometrias de la particion
				if auxGeoPartIdx == 0
					auxGeoPartIdx = i + (j - 1)
				else
					auxGeoPartIdx += 1
				end
				puts "Indice auxGeo: #{auxGeoPartIdx}"
				
				theta0 = theta - (j-1) * offset
  					
  				#Buscamos la particion generada a partir de los datos
  				auxpartition = extract_partition_from_mesh(collection, v1, theta, xmax, ymax, v1,v2,v3,i,true)
                                 
				diffparts = diffparts.concat(auxpartition)
				auxpartitions << auxpartition
  				auxGeoPart.merge!({auxGeoPartIdx => { "vertex" => [v1,v2, v3], "offset" => offset, "theta" => theta, "theta0" => theta0}})
			end	
			#Verificamos que se dividio la misma cantidad de elementos
			diffpart = part - diffparts
			#Si la cantidad de elementos es menor a la esperada la guarda para luego decidir de manera global donde asignarlos

			unless diffpart.empty?
				sumdiffs += diffpart.size
				tofixedelements[i] = {"elements" => diffpart, "geo" => [*auxGeoPartIdx..(auxGeoPartIdx-npart)], "nelems" => sumdiffs}	
			        
			end
		else
			puts "*********** No reparticionar #{i} *************"
			puts "Cantidad elementos #{current_size}"
			#Siempre el inicial original (NO CAMBIAR)
			theta = thetaOriginal
			#El movimiento de theta0 lo hacemos al cambiar v2 con v3 eso ya me 
			puts "Theta refinamiento: #{theta}"
			
			#Si ninguna particion se reparticiono porque se encontro el criterio
			#el orden es el mismo como estaba originalmente
			if auxGeoPartIdx == 0
				auxGeoPart.merge!({i => geoPart[i]})
			else 	
				#En caso de haber habido un cambio, se reconfiguraron los indices
				#por lo que tengo que sumarle uno al ultimo calculado
				auxGeoPartIdx += 1
				auxGeoPart.merge!({auxGeoPartIdx => geoPart[i]})
			end
			auxpartitions << part
		end
	end
	seq_elems = []
#	tofixedelements[i] = {"elements" => diffpart, "geo" => [*auxGeoPartIdx..(auxGeoPartIdx-npart)], "nelems" => sumdiffs}	
	if sumdiffs > 0	        
		puts "Faltaron #{sumdiffs}"
		puts "Procedemos a corregir a los elementos"
		#Armamos los grupos de procesamiento
		totalParticiones = auxpartitions.size
		groupsize = totalParticiones / nproc
		groupsPartitions = auxpartitions.each_slice(groupsize).to_a
		
		#Analizo cada uno de los elementos. Como sabemos que ese elemento antes pertenecia a una particion en particular podemos solo analizar las subparticiones generadas a partir de la original	
		tofixedelements.each do |k,v|
			geo = v["geo"]
			elements = v["elements"]
			#Tenemos cada uno de los numeros de elementos
			#Buscamos las coordenadas en la base de datos
			
			elements.each do |e|
				db_element = get_element_from_db(collection, e) 
				#Buscamos a que particiones pertenecen las aristas
				partitions_contains = []

				belongs_to_partition = Hash.new { |h,k| h[k] = [] }
				geo.each do |gi|
					vert1, vert2,vert3 = auxGeoPart[gi]["vertex"]
 					n1,n2,n3 = element_nodes_included_in_triangle(db_element, vert1,vert2,vert3)
					if n1 || n2 || n3
						#Me fijo que procesador procesaria la particion gi.
						gproc = gi / groupsize
						belongs_to_partition[gproc] << gi
					end
				end

				if belongs_to_partition.size == 1	
				#Caso 1 si las aristas perteneces a subparticiones que van a ser procesadas por un mismo procesador
					gproc = belongs_to_partition.keys.first
					gnp = (gproc - 1) * groupsize
					auxpartitions[gnp].push(e)
				else
				#Caso 2 si las aristas pertenecen a subparticiones que van a ser procesadas por diferentes procesadores
				#Analizo los diferentes ordenes a ser procesados
					orders = Hash.new { |h,k| h[k] = false }
					belongs_to_partition.each do |idxs|
						idxs.each do |o|
							orders[o % groupsize] = true
						end	
					end

					if orders.size == 1
					#Caso 2.1 Si va ser procesado en el mismo momento (orden) por dos procesadores diferentes
						orden = orders.keys.first
						gnp = orden + 1
						auxpartitions[gnp].push(e)
					else
					#Caso 2.2 Si va ser procesado en diferente orden por diferentes procesadores 
						puts "El elemento #{e} no puede ser procesado con el tamaño de elemento actual. Se recomienda dividirlo en partes y volver a generar las particiones"
						puts "Otra opcion es agregar la lista del archivo faltantes como ultima coleccion a procesar. Se debera modificar el programa para que se procese de manera secuencial luego de procesar todo lo paralelo"
						seq_elems << e
					end
				end

			end
		end
	end
	
	#open("seq.out","a") do |f|
	#	f.puts "***** #{nstep} *****"
	#	f.puts seq_elems.join(" ")
	#end
	
	qpElements, partitions, intersection_is_empty = validate_partitions(auxpartitions, 4400)
	puts "PASO interno #{nstep}"
	puts "Verificacion de elementos: #{qpElements == 4400}"
	puts "Verificacion interseccion: #{intersection_is_empty}"
	
	return refine_partitions(collection, auxpartitions, seq_elems, auxGeoPart, radio, xmax,ymax, nproc, delta, nstep + 1, nmaxsteps)
  end
end

execute(collection, 4, factor, _nmaxsteps: 1)
