require 'awesome_print'
require 'mongo'

DEBUG = false

#Conexion a la base de datos de coordenadas y topologia de la malla
client = Mongo::Client.new([ '127.0.0.1:27017'  ], :database => 'c181_islaDensificada')
db = client.database
collection = client[:elements]

#Funciones
def get_partition(collection, min,max, xmax, ymax, center)
        centerx = center["x"]
        centery = center["y"]

        collection.find(
                {
                        :$or => [
                                {
                                  :$and => [
                                        {
                                                :"nodes.node1.x" => { :$regex => /^200\.00.*$/}
                                        },
                                        {
                                                :"nodes.node2.x" => { :$regex => /^200\.00.*$/}
                                        }
                                  ]
                                },
                                {
                                  :$and => [
                                        {
                                                :"nodes.node1.x" => { :$regex => /^200\.00.*$/}
                                        },
                                        {
                                                :"nodes.node3.x" => { :$regex => /^200\.00.*$/}
                                        }
                                  ]
                                },
                                {
                                  :$and => [
                                        {
                                                :"nodes.node2.x" => { :$regex => /^200\.00.*$/}
                                        },
                                        {
                                                :"nodes.node3.x" => { :$regex => /^200\.00.*$/}
                                        }
                                  ]
                                }
                        ]
                },
                {
                        :element_number => 1
                }
        )

end

#Calcula la coordenadas del tercer vertice del triangulo
def find_vertex(radio,center, theta)
  x = radio * Math.cos(theta) + center["x"]
  y = radio * Math.sin(theta) + center["y"]

  {"x" => x,"y" => y}
end

#Elimina duplicados entre particiones. La particion que elije para eliminar
#en caso de encontrar duplicados es la que tiene mayor cantidad de elementos
def remove_duplicates(partitions)
  partitions
end

def get_search_space(collection, center,theta,xmax,ymax)
  collection.find({})
end

#Funcion auxiliar para conocer si un punto pertenece a un triangulo
def sign(p1,p2,p3)
  a1 = (p1["x"] - p3["x"])
  ap a1 if DEBUG
  a2 = (p2["y"] - p3["y"])
  ap a2 if DEBUG
  a = a1 * a2
  ap a if DEBUG

  b1 = (p2["x"] - p3["x"])
  ap b1 if DEBUG
  b2 = (p1["y"] - p3["y"])
  ap b2 if DEBUG
  b = b1 * b2
  ap b if DEBUG
  a - b
end

def included_in_triangle?(node, a,b,c)
  ap "node: #{node}" if DEBUG
  ap "a: #{a}" if DEBUG
  ap "b: #{b}" if DEBUG
  ap "c: #{c}" if DEBUG

  b1 = sign(node, a,b) < 0.0
  b2 = sign(node, b,c) < 0.0
  b3 = sign(node, c,a) < 0.0

  b1 == b2 && b2 == b3
end

def verify_partitions(part)
  v = true

  part.each do |p|
     sub = part - [p]
     sub.each do |p2|
      v = p & p2 == []
      return v if !v
     end
     return v if !v
  end

  return v
end

def execute(collection,np = nil, fpart = nil)
#Variables del problema
nprocs = np || 8
factorproc = fpart || 1
npartitions = factorproc * nprocs #esto podria cambiar si quiero hacer el interleave
pi = Math::PI
offset = (2.0 * pi)/npartitions
xmax = 200.0
ymax = 40.0
partitions = []
nelems = 4400
center = {"x" => xmax / 2.0, "y" => ymax / 2.0}

coeffr = 1.0 #indica una proporcion del tamaño de la malla como radio
radio = xmax * coeffr

#En realidad es el vertice 2 que en principio se propone pero para evitar
#una asignacion de mas lo definimos como el tercer vertice asignandolo como
#se hara para las particiones consecutivas
vert3 = find_vertex(radio, center, 0.0)

1.upto(npartitions) do |i|
  theta = i * offset
  vert1 = center
  vert2 = vert3
  vert3 = find_vertex(radio, center, theta)
  ap "Vertices #{vert1} #{vert2} #{vert3}"
  #Obtenemos el espacio de busqueda reducido
  search_space = get_search_space(collection, center, theta, xmax, ymax)
  #Analizamos cada elemento del espacio de busqueda. Si al menos dos de sus
  #nodos pertenecen al triangulo definido con vertices (vert1,vert2,vert3)
  #Lo incluimos en la particion. De lo contrario lo descartamos
  current_partition = []

  search_space.each do |element|
    #Obtenemos los nodos del elemento
      node1 = element["nodes"]["node1"]
      node2 = element["nodes"]["node2"]
      node3 = element["nodes"]["node3"]

      node1 = {"x" => node1["x"].to_f, "y" => node1["y"].to_f}
      node2 = {"x" => node2["x"].to_f, "y" => node2["y"].to_f}
      node3 = {"x" => node3["x"].to_f, "y" => node3["y"].to_f}
      ap node1 if DEBUG
      ap node2 if DEBUG
      ap node3 if DEBUG

      if node1.nil? or node2.nil? or node3.nil?
        puts "El elemento #{element} tiene nulos"
        next
      end
      #Analizamos el primer nodo 
      q1 = included_in_triangle?(node1, vert1,vert2, vert3)
      #Analizamos el segundo nodo 
      q2 = included_in_triangle?(node2, vert1,vert2, vert3)
      #Analizamos el tercer nodo 
      q3 = included_in_triangle?(node3, vert1,vert2, vert3)

      is_included = (q1 && q2) || (q1 && q3) || (q2 && q3)
      if(is_included)
        current_partition << element["element_number"].to_i
      end
    end

    partitions << current_partition
  end

  #Agregar chequeo que tienen que ser 4400 elementos todas las particiones
  qpElements =  partitions.reduce(0) {|acc, e| acc+=e.size }

  #Si no tiene la misma cantidad de elementos elimino duplicados
  #tengo que verlo porque podria dar la suma de elementos y no estar todos los elementos?
  #en principio deberia de haber elementos de mas
  if qpElements != nelems
   partitions = remove_duplicates(partitions)
  end
  #ordenamos para que las particiones con cantidad de elementos similares
  #queden juntas
  #lpartitions = partitions.sort_by {|e| e.size }.reverse

  #imprimimos un archivo con los elementos de cada particion
  filename = "partitions.txt"
  File.open(filename, "w") do |f|
    f.write("#{nprocs} #{npartitions}\n")
    lines = partitions.map {|e| "#{e.size} #{e.sort.join(' ')}"}
    lines.each do |l|
      f.write("#{l}\n")
    end
  end
  #Realizamos una verificacion para ver si todo va bien. 
  intersection_is_empty = verify_partitions(partitions)
  #Imprimimos un resumen de los realizado
  ap "Numero de procesadores #{nprocs}"
  ap "Numero de particiones #{npartitions}"
  i = 1
  partitions.each do |p|
    ap "Particion ##{i}"
    ap "Cantidad de elementos: #{p.size}"
    i += 1
  end
  ap "Suma de elementos de las particiones: #{qpElements}"
  ap "La interseccion es vacia #{intersection_is_empty}"
  ap "================================="
  true
end

execute(collection, 4, 4)
