require 'awesome_print'
require 'mongo'

client = Mongo::Client.new([ '127.0.0.1:27017'  ], :database => 'c181_islaDensificada')
db = client.database
collection = client[:elements]

def get_left_border_nodes(collection)
        n1 = collection.find({:"nodes.node1.x" => { :$regex => /^0\.00.*$/}}).map { |e| e["nodes"]["node1"]["number"].to_i}
        n2 = collection.find({:"nodes.node2.x" => { :$regex => /^0\.00.*$/}}).map { |e| e["nodes"]["node2"]["number"].to_i}
        n3 = collection.find({:"nodes.node3.x" => { :$regex => /^0\.00.*$/}}).map { |e| e["nodes"]["node3"]["number"].to_i}
        n1.concat(n2).concat(n3).uniq.sort
end

def get_right_border_nodes(collection)
        n1 = collection.find({:"nodes.node1.x" => { :$regex => /^200\.00.*$/}}).map { |e| e["nodes"]["node1"]["number"].to_i}
        n2 = collection.find({:"nodes.node2.x" => { :$regex => /^200\.00.*$/}}).map { |e| e["nodes"]["node2"]["number"].to_i}
        n3 = collection.find({:"nodes.node3.x" => { :$regex => /^200\.00.*$/}}).map { |e| e["nodes"]["node3"]["number"].to_i}
        n1.concat(n2).concat(n3).uniq.sort
end

def get_top_border_nodes(collection)
        n1 = collection.find({:"nodes.node1.y" => { :$regex => /^40\.00.*$/}}).map { |e| e["nodes"]["node1"]["number"].to_i}
        n2 = collection.find({:"nodes.node2.y" => { :$regex => /^40\.00.*$/}}).map { |e| e["nodes"]["node2"]["number"].to_i}
        n3 = collection.find({:"nodes.node3.y" => { :$regex => /^40\.00.*$/}}).map { |e| e["nodes"]["node3"]["number"].to_i}
        n1.concat(n2).concat(n3).uniq.sort
end


def get_bottom_border_nodes(collection)
        n1 = collection.find({:"nodes.node1.y" => { :$regex => /^0\.00.*$/}}).map { |e| e["nodes"]["node1"]["number"].to_i}
        n2 = collection.find({:"nodes.node2.y" => { :$regex => /^0\.00.*$/}}).map { |e| e["nodes"]["node2"]["number"].to_i}
        n3 = collection.find({:"nodes.node3.y" => { :$regex => /^0\.00.*$/}}).map { |e| e["nodes"]["node3"]["number"].to_i}
        n1.concat(n2).concat(n3).uniq.sort
end

r =  get_left_border_nodes(collection)
puts "Cantidad de nodos izquierda #{r.size}"
r.each {|e| puts e}


r =  get_right_border_nodes(collection)
puts "Cantidad de nodos derecha #{r.size}"
r.each {|e| puts e}

r =  get_top_border_nodes(collection)
puts "Cantidad de nodos superiores #{r.size}"
r.each {|e| puts e}

r =  get_bottom_border_nodes(collection)
puts "Cantidad de nodos inferiores #{r.size}"
r.each {|e| puts e}
